# -*- coding: utf-8 -*-
###############################################################################
#
#    Tech-Receptives Solutions Pvt. Ltd.
#    Copyright (C) 2009-TODAY Tech-Receptives(<http://www.techreceptives.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

{
    'name': 'Shoe Embassy',
    'version': '1.0.0',
    'license': 'LGPL-3',
    "sequence": 3,
    'complexity': "easy",
    'description': """


    """,
    'author': 'Anywhereconnex',
    'website': '',
    'depends': ['crm',
                'point_of_sale', 'product', 'purchase',
                'sale', 'sales_team', 'stock', 'board'],
    'data': [
        'security/ir.model.access.csv',
        'views/shoe_embassy_app.xml',
        'views/product_view.xml',
        'views/orderpoint_view.xml',
        'views/warehouse_view.xml',
        'views/res_partner_view.xml',
        'views/import_view.xml',
        'views/models_view.xml',
        'views/size_correlation_view.xml',
        # 'report/pos_order_report_view.xml',
        'report/sale_order_report_view.xml',
        'data/product_data.xml',
        'data/cron_data.xml',
        'data/size_correlation_data.xml',

    ],
    'demo': [    ],
    'qweb': ['static/src/xml/board.xml'],
    'images': [    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
