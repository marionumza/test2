# -*- coding: utf-8 -*-


from odoo import models, fields, osv, api
from odoo.exceptions import ValidationError
from datetime import datetime, date
import os

from odoo.tools.translate import _
from odoo.osv import osv
import json
import ast

import csv
import base64


class ShoeEmbassyProductImport3DStock(models.Model):
    _name = "shoe.embassy.product.import3.dstock"

    name = fields.Char('Note')
    file = fields.Binary('Import Dear Stock File')
    filename = fields.Char()

    def shoe_embassy_product_import_from_csv(self):

        fo = open('/tmp/shoe_embassy_product3.csv', 'wb+')
        fo.write(base64.b64decode(self.file))
        fo.close()
        f = open('/tmp/shoe_embassy_product3.csv')
        csv_f = csv.reader(f)
        cont = 1
        headers = []
        for row in csv_f:
            if cont == 1:
                cont += 1
                for r in row:
                    headers.append(r)
                continue
            else:
                cell_count = 0
                product_vals = {}
                stock_vals = {}
                reorder_vals = {}
                supplier_vals = {}
                print str(cont) + ' ' + str(row[cell_count])

                # Location
                cell_count += 1

                # Warehouse
                warehouse = self.env['stock.warehouse'].search([('name', '=', unicode(row[cell_count-1]))])
                if row[cell_count] and not warehouse.id:
                    warehouse = self.env['stock.warehouse'].create({'name': unicode(row[cell_count-1]), 'code': row[cell_count].decode('utf-8')})
                stock_vals.update({'location_id': warehouse.lot_stock_id.id or False})
                cell_count += 1

                # SKU
                # product = self.env['product.template'].search([('default_code', '=', unicode(row[cell_count]))])
                # product_product = self.env['product.product'].search([('product_tmpl_id', '=', product.id)])
                product_product = self.env['product.product'].search([('barcode', '=', unicode(row[cell_count]))])
                product = product_product.product_tmpl_id
                reorder_vals.update({'product_id': product_product.id or False})
                stock_vals.update({'product_id': product_product.id or False})
                # supplier_vals.update({'product_id': product_product.id or False})
                supplier_vals.update({'product_tmpl_id': product.id or False})
                cell_count += 1

                # Product
                cell_count += 1

                # Unit
                cell_count += 1

                # Quantity on Hand
                stock_vals.update({'qty': unicode(row[cell_count])})
                cell_count += 1

                # Allocated
                cell_count += 1

                # On Order
                cell_count += 1

                # Unit Cost
                # if product.id in price_list.keys():
                #     if unicode(row[cell_count]) < price_list[product.id]:
                #         price_list[product.id] = row[cell_count]
                # else:
                #     price_list[product.id] = row[cell_count]
                # product_vals.update({'standard_price_price': price_list[product.id]})
                # extra_price = float(row[cell_count]) - float(price_list[product.id])
                cell_count += 1

                # Stock on Hand
                cell_count += 1

                # Available
                cell_count += 1

                # Reorder Level
                # reorder_vals.update({'product_min_qty': unicode(row[cell_count])})
                #
                # cell_count += 1

                # Restock Level
                # reorder_vals.update({'product_max_qty': unicode(row[cell_count])})
                # reorder_vals.update({'qty_multiple': 0})
                # cell_count += 1

                # Last Supplier
                # supplier_partner = self.env['res.partner'].search([('name', '=', unicode(row[cell_count]))])
                # if len(supplier_partner) == 0:
                #     supplier_partner = self.env['res.partner'].create({
                #         'name': unicode(row[cell_count]), 'is_company': True})
                # supplier_vals.update({'name': supplier_partner.id or False})
                #
                # supplier = self.env['product.supplierinfo'].search([
                #     ('name', '=', supplier_partner.id),('product_tmpl_id', '=', product.id)])
                # if len(supplier) == 0:
                #     supplier = self.env['product.supplierinfo'].create(supplier_vals)
                # cell_count += 1

                # Tax Rule
                # tax = self.env['account.tax'].search([('amount', '=', row[cell_count][:2]),('type_tax_use', '=', 'sale')])
                # if row[cell_count] and not tax.id:
                #     tax = self.env['account.tax'].create({
                #         'amount': unicode(row[cell_count][:2]),
                #         'name': unicode(row[cell_count]),
                #         'type_tax_use': 'sale'})
                # product_vals.update({'taxes_id': [tax.id or False]})
                # cell_count += 1

                # PRODUCT  block
                # product = self.env['product.template'].search([('default_code', '=', product_vals['default_code'])])
                if len(product) == 0:
                    cont += 1
                    print ' Not imported '
                    continue
                else:
                    product.write(product_vals)

                # orderpoint creation block
                # reorder_vals.update({'lead_type': 'net'})
                # orderpoint = self.env['stock.warehouse.orderpoint'].search([
                #     ('product_id', '=', product.id),
                #     ('location_id', '=', warehouse.lot_stock_id.id)])
                # if len(orderpoint) == 0:
                #     orderpoint = self.env['stock.warehouse.orderpoint'].create(reorder_vals)
                #
                # else:
                #     orderpoint.write(reorder_vals)

                # stock.quant creation block
                stock = self.env['stock.quant'].search([
                    ('product_id', '=', product_product.id), ('location_id', '=', warehouse.lot_stock_id.id)])
                if len(stock) == 0:
                    stock = self.env['stock.quant'].create(stock_vals)
                else:
                    stock.write(stock_vals)

                # price block
                # if extra_price > 0:
                #     value_id = product_product.attribute_value_ids.id
                #     price = self.env['product.attribute.price'].search([
                #         ('product_tmpl_id', '=', product.id),
                #         ('value_id', '=', value_id)
                #     ])
                #     if len(price) == 0:
                #         vals = {
                #             'product_tmpl_id': product.id,
                #             'value_id': value_id,
                #             'price_extra': extra_price}
                #         price = self.env['product.attribute.price'].create(vals)
                #     else:
                #         price.write({'price_extra': extra_price})

            cont += 1
            error_count = 0

        os.remove('/tmp/shoe_embassy_product3.csv')
        return True


