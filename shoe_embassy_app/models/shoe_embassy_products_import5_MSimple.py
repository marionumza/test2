# -*- coding: utf-8 -*-


from odoo import models, fields, osv, api
from odoo.exceptions import ValidationError
from datetime import datetime, date
import os


from odoo.tools.translate import _
from odoo.osv import osv
import json
import ast

import csv
import base64

class ShoeEmbassyProductImport5MSimple(models.Model):
    _name = "shoe.embassy.product.import5.msimple"

    name = fields.Char('Note')
    file = fields.Binary('Import MSimple File')
    filename = fields.Char()

    

    def shoe_embassy_product_import_from_csv(self):

        fo = open('/tmp/shoe_embassy_product5.csv', 'wb+')
        fo.write(base64.b64decode(self.file))
        fo.close()
        f = open('/tmp/shoe_embassy_product5.csv')
        csv_f = csv.reader(f)
        cont = 1
        headers = []
        error_count = 0
        for row in csv_f:
            # print cont
            if cont == 1:
                cont += 1
                for r in row:
                    headers.append(r)
                continue
            else:
                cell_count = 0
                product_vals = {}
                product_product_vals = {}

                #item - id
                product_product_vals.update({'simple_id': unicode(row[cell_count])})
                cell_count += 1


                # sku
                product_product_vals.update({'barcode': unicode(row[cell_count])})
                cell_count += 1


                #configurable - id
                product_vals.update({'conf_id': unicode(row[cell_count])})
                cell_count += 1


                #configurable - sku
                product_name = unicode(row[cell_count])
                cell_count += 1

                product_product = self.env['product.product'].search([('barcode', '=', product_product_vals['barcode'])])
                if len(product_product) == 0:
                    if product_vals['conf_id']:
                        product = self.env['product.template'].search([('conf_id', '=', product_vals['conf_id'])])
                        if len(product) == 0:
                            product_vals.update({'name': product_name})
                            product = self.env['product.template'].create(product_vals)
                            product_product = self.env['product.product'].search([('product_tmpl_id', '=', product.id)])
                            if len(product_product) == 0:
                                product_product_vals.update({
                                    'barcode': False,
                                    'product_tmpl_id': product.id,
                                    'simple_id': product_product_vals['simple_id']
                                })
                                product_product = self.env['product.product'].create(product_product_vals)
                            else:
                                product_product_vals.update({
                                    'barcode': False,
                                    'simple_id': product_product_vals['simple_id']
                                })
                                product_product.write(product_product_vals)
                        else:
                            product_product = self.env['product.product'].search([
                                ('product_tmpl_id', '=', product.id),
                                ('simple_id', '=', product_product_vals['simple_id'])])
                            if len(product_product) == 0:
                                product_product_vals.update({
                                    'barcode': False,
                                    'product_tmpl_id': product.id,
                                    'simple_id': product_product_vals['simple_id']
                                })
                                product_product = self.env['product.product'].create(product_product_vals)
                            else:
                                product_product.write(product_product_vals)
                    else:
                        print 'Simple id ' + product_product_vals['simple_id']

                else:
                    product_product.write(product_product_vals)
                    product_product.product_tmpl_id.write(product_vals)

            cont += 1

        os.remove('/tmp/shoe_embassy_product5.csv')
        return True


