# -*- coding: utf-8 -*-
# Part of Softhealer Technologies.

import json
import werkzeug
import itertools
import pytz
import babel.dates
from collections import OrderedDict

from odoo import http, fields, _
from odoo.addons.website.controllers.main import QueryURL
from odoo.addons.website.models.website import slug, unslug
from odoo.exceptions import UserError
from odoo.http import request
from odoo.tools import html2plaintext


class website_faq(http.Controller):
    @http.route([
        '/page/faq',
    ], type='http', auth="public", website=True)
    def faq(self, **post):
        faq_obj = request.env['website.faq']
        search_faqs = faq_obj.search([('active','=',True),('faq_lines','!=',False)])
        return request.render("sh_website_faq.faq", {'faqs':search_faqs})
   