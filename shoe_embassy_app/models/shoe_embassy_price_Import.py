# -*- coding: utf-8 -*-


from odoo import models, fields, osv, api
from odoo.exceptions import ValidationError
from datetime import datetime, date
import os


from odoo.tools.translate import _
from odoo.osv import osv
import json
import ast

import csv
import base64

class Shoe_Embassy_Price_import(models.Model):
    _name = "shoe.embassy.price.import"

    name = fields.Char('Note')
    file = fields.Binary('Import Price File')
    filename = fields.Char()

    

    def shoe_embassy_price_import_from_csv(self):

        fo = open('/tmp/shoe_embassy_price.csv', 'wb+')
        fo.write(base64.b64decode(self.file))
        fo.close()
        f = open('/tmp/shoe_embassy_price.csv')
        csv_f = csv.reader(f)
        cont = 1
        headers = []
        for row in csv_f:
            # print cont
            if cont == 1:
                cont += 1
                for r in row:
                    headers.append(r)
                continue
            else:
                cell_count = 0
                message = ''

                # item - id
                product = self.env['product.template'].search([('conf_id', '=', row[cell_count])])
                product_product = self.env['product.product'].search([('product_tmpl_id', '=', product.id)])
                if len(product) > 1:
                    raise ValidationError(_("There are more then one product with magento ID " + row[cell_count]))
                elif len(product) == 1:
                    cell_count += 1

                    # sku
                    # print row[cell_count - 1] + ' ' + row[cell_count]
                    cell_count += 1

                    # name
                    cell_count += 1

                    # price
                    product.write({'list_price': row[cell_count]})
                    cell_count += 1

                    # special_price
                    if row[cell_count]:
                        pricelist = self.env['product.pricelist'].search([('name', '=', 'Special Price')])
                        if len(pricelist) == 0:
                            pricelist = self.env['product.pricelist'].create({
                                'name': 'Special Price',
                                'selectable': True,
                                'discount_policy': 'with_discount',
                            })
                        item = self.env['product.pricelist.item'].search([
                            ('product_tmpl_id', '=', product.id),
                            ('pricelist_id', '=', pricelist.id)
                        ])
                        if len(item) == 0:
                            product.write({'item_ids': [(0, 0, {
                                'applied_on': '1_product',
                                'product_tmpl_id': product.id,
                                'min_quantity': 1,
                                'compute_price': 'fixed',
                                'fixed_price': row[cell_count],
                                'pricelist_id': pricelist.id,
                                'base': 'list_price',
                            })]})
                        else:
                            item.write({
                                'applied_on': '1_product',
                                'min_quantity': 1,
                                'compute_price': 'fixed',
                                'fixed_price': row[cell_count],
                                'base': 'list_price',
                            })
                        product_product.write({'special_price': row[cell_count]})

                else:
                    print row[cell_count] + ' ' + row[cell_count + 1] + ' Not Found'


            cont += 1

        os.remove('/tmp/shoe_embassy_price.csv')
        self.write({'name': message})
        return True


