# -*- coding: utf-8 -*-


from odoo import models, fields, osv, api
from odoo.exceptions import ValidationError
from datetime import datetime, date
import os


from odoo.tools.translate import _
from odoo.osv import osv
import json
import ast

import csv
import base64

class Shoe_Embassy_Vendors_import(models.Model):
    _name = "shoe.embassy.vendors.import"

    name = fields.Char('Note')
    file = fields.Binary('Import Vendors File')
    filename = fields.Char()

    

    def shoe_embassy_vendors_import_from_csv(self):

        fo = open('/tmp/shoe_embassy_vendors.csv', 'wb+')
        fo.write(base64.b64decode(self.file))
        fo.close()
        f = open('/tmp/shoe_embassy_vendors.csv')
        csv_f = csv.reader(f)
        cont = 1
        headers = []
        for row in csv_f:
            print cont
            if cont == 1:
                cont += 1
                for r in row:
                    headers.append(r)
                continue
            else:
                cell_count = 0
                message = ''
                values = {}

                # item - id SKU
                product_product = self.env['product.product'].search([('barcode', '=', row[cell_count])])
                if len(product_product) > 1:
                    raise ValidationError(_("There are more then one product with barcode " + row[cell_count]))
                elif len(product_product) == 1:
                    cell_count += 2 # PRODUCT

                    # supplier
                    partner_name = row[cell_count].decode('utf-8')
                    partner = self.env['res.partner'].search([
                        ('name', '=', row[cell_count].decode('utf-8'))])
                    if len(partner) == 0:
                        partner = self.env['res.partner'].create({'name': row[cell_count].decode('utf-8')})
                    cell_count += 1

                    # Supplier SKU
                    values.update({'product_code': row[cell_count]})
                    cell_count += 2

                    # price
                    values.update({'min_qty': 1, 'price': row[cell_count]})
                    cell_count += 1

                    if len(partner) > 1:
                        raise ValidationError(_('Error! There are two partners with the name ' + partner_name))
                    supplier = self.env['product.supplierinfo'].search([
                        ('name', '=', partner.id),
                        ('product_tmpl_id', '=', product_product.product_tmpl_id.id)
                    ])
                    if len(supplier) == 0:
                        supplier = self.env['product.supplierinfo'].create({
                            'name': partner.id,
                            'product_tmpl_id': product_product.product_tmpl_id.id,
                            'min_qty': values['min_qty'],
                            'price': values['price'],
                            'delay': 1,
                            'product_code': values['product_code'],
                        })
                    else:
                        supplier.write({
                            'min_qty': values['min_qty'],
                            'price': values['price'],
                            'delay': 1,
                            'product_code': values['product_code'],
                        })
                        print supplier.id

                else:
                    print row[cell_count] + ' Not Found'


            cont += 1

        os.remove('/tmp/shoe_embassy_vendors.csv')
        return True


