odoo.define('Shoeembassy_Hero.footer_data', function (require) {
'use strict';
var ajax = require('web.ajax');
var core = require('web.core');
var base = require('web_editor.base');
var animation = require('web_editor.snippets.animation');
var no_of_product;
var qweb = core.qweb;
animation.registry.js_get_category=animation.Class.extend({selector:".js_get_category",
	start:function(){
		this.redrow()},
	stop:function(){
		this.clean()},
	redrow:function(t){
		this.clean(t),
		this.build(t)},
	clean:function(t){
		this.$target.empty()},
	build: function(debug)
    {
      	var self = this;
      	var template = self.$target.data("template");
      	if(!template) template="category_image_showcase_snippent.footer_data_for_loop";
	  	var rpc_end_point = '/showfooter_data';
      	ajax.jsonRpc(rpc_end_point, 'call', {
        	'template': template,
      	}).then(function(data){
      	  self.$target.empty();
    	  $(data).appendTo(self.$target);
      	}).then(function(){
      }).fail(function(e){
        return;
      });
    },
});
});