# -*- coding: utf-8 -*-
from odoo import api, models, tools, registry, fields, _
import threading
from collections import defaultdict
from datetime import datetime
from dateutil.relativedelta import relativedelta
from psycopg2 import OperationalError

from odoo import api, fields, models, registry, _
from odoo.osv import expression
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, float_compare, float_round
import logging
from pygments.lexer import _inherit
_logger = logging.getLogger(__name__)


class Gender(models.Model):
    _name = 'gender'

    name = fields.Char(string='Gender')

    _sql_constraints = [
        ('unique_name', 'UNIQUE(name)',
         'You can only add one time each Gender.')
    ]


class ProcurementGroup(models.Model):
    _inherit = 'procurement.group'

    @api.multi
    def name_get(self):
        result = []
        for record in self:
            if record.default:
                result.append((record.id, record.name + '(Default)'))
            elif record.gender_id:
                result.append((record.id, record.name + '(' + record.gender_id.name + ')'))
            else:
                result.append((record.id, record.name))
        return result

    gender_id = fields.Many2one('gender', string='Order Group')
    default = fields.Boolean(string="Default")


class OrderpointPeriod(models.Model):
    _name = 'orderpoint.period'

    @api.multi
    def name_get(self):
        result = []
        for record in self:
            if record.default:
                result.append((record.id, record.name + '(Default)'))
            elif record.gender_id:
                result.append((record.id, record.name + '(' + record.gender_id.name + ')'))
            else:
                result.append((record.id, record.name))
        return result

    def compute_available_suppliers(self):
        self.env.cr.execute("""
            SELECT DISTINCT(name) from product_supplierinfo
            UNION
            SELECT DISTINCT(vendor_id) from orderpoint_period
        """)
        result = self.env.cr.fetchall()
        supplier_list = []
        for res in result:
            supplier_list.append(res[0])
        return self.env['res.partner'].search([('id', 'in', supplier_list)])

    name = fields.Char('Period Name')
    gender_id = fields.Many2one('gender', string='Order Group')
    default = fields.Boolean(string="Default")
    supplier_info_ids = fields.Many2many('res.partner', default=compute_available_suppliers)
    vendor_id = fields.Many2one('res.partner', string='By Supplier')


class StockLocationRoute(models.Model):
    _inherit = "stock.location.route"

    is_stock_location = fields.Boolean('Stock Locations')
    location_ids = fields.Many2many('stock.location',string='Stock Locations')
    procurement_id = fields.Many2one('procurement.group',string='Default Procurement Group')
    procurement_ids = fields.Many2many('procurement.group',string='Procurement Groups')
    preferred_destination_warehouse_id = fields.Many2one('stock.warehouse',string='Start Source Warehouse')
    procurement_warehouse_id = fields.Many2one('stock.warehouse','Procurement Warehouse')


class Warehouse(models.Model):
    _inherit = "stock.warehouse"
    _name = "stock.warehouse"

    period_id = fields.Many2one('orderpoint.period', string='Default Orderpoint Period')
    period_ids = fields.Many2many('orderpoint.period', string='Orderpoint Periods')
#     in_transfer_location_id = fields.Many2one('stock.location','Location for Internal Transfer')
    location_ids = fields.Many2many('stock.location',string='Stock Locations')
    stock_picking_type = fields.Many2one('stock.picking.type', string='Default Operation Type')
    procurement_id = fields.Many2one('procurement.group',string='Default Procurement Group')
    procurement_ids = fields.Many2many('procurement.group',string='Procurement Groups')
    route_id = fields.Many2one('stock.location.route', string='Default Route on Reclaim')


class StockPicking(models.Model):
    _inherit = "stock.picking"
    
    @api.model
    def auto_reserve_picking_cron_method(self):
        """ Must be called from ir.cron """
        picking_ids = self.env['stock.picking'].search([('state','=','confirmed'),('name','ilike','/PICK/')])
        if picking_ids: 
            picking_ids.action_assign()
        return True


class Orderpoint(models.Model):
    _inherit = "stock.warehouse.orderpoint"
    _name = "stock.warehouse.orderpoint"

    @api.model
    def _get_default_period(self):
         return self.warehouse_id.period_id.id or False

    # @api.one
    # @api.depends('product_id')
    # def _get_period(self):
    #     gender = self.product_id.gender
    #     if gender:
    #         gender_id = self.env['gender'].search([('name', '=', gender)])
    #         period_id = self.warehouse_id.period_ids.search([('gender_id', '=', gender_id.id)])
    #         if period_id:
    #             self.period_id = period_id.id
    
    @api.onchange('product_id')
    def onchange_product_id(self):
        gender = self.product_id.gender
        if gender:
            gender_id = self.env['gender'].search([('name', '=', gender)])
            period_id = self.env['orderpoint.period'].search([('gender_id', '=', gender_id.id),
                                                              ('id', 'in', self.warehouse_id.period_ids.ids)])
            if period_id:
                self.period_id = period_id.id
        elif self.product_id.gender_id.id:
            period_id = self.env['orderpoint.period'].search([('gender_id', '=', self.product_id.gender_id.id),
                                                              ('id', 'in', self.warehouse_id.period_ids.ids)])
            if period_id:
                self.period_id = period_id.id
        return super(Orderpoint, self).onchange_product_id()

#     @api.model
#     def _get_default_in_transfer_location_id(self):
#         return self.warehouse_id.in_transfer_location_id and self.warehouse_id.in_transfer_location_id.id or False
    
    @api.onchange('warehouse_id')
    def onchange_warehouse_id(self):
        """ Finds location id for changed warehouse. """
        if self.warehouse_id:
            self.location_id = self.warehouse_id.lot_stock_id.id
            self.period_id = self.warehouse_id.period_id.id
#             self.in_transfer_location_id = self.warehouse_id.in_transfer_location_id and self.warehouse_id.in_transfer_location_id.id or False

    period_id = fields.Many2one('orderpoint.period', string='Orderpoint Period', default=_get_default_period)
    warehouse_ids = fields.Many2many('stock.warehouse', string='Warehouses to the calculations')
#     in_transfer_location_id = fields.Many2one('stock.location','Location for Internal Transfer',default=_get_default_in_transfer_location_id)
    
#     @api.multi
#     def _prepare_procurement_values(self, product_qty, date=False, group=False):
#         return {
#             'name': self.name,
#             'date_planned': date or self._get_date_planned(datetime.today()),
#             'product_id': self.product_id.id,
#             'product_qty': product_qty,
#             'company_id': self.company_id.id,
#             'product_uom': self.product_uom.id,
#             'location_id': self.in_transfer_location_id and self.in_transfer_location_id.id or False, 
#             'origin': self.name,
#             'warehouse_id': self.warehouse_id.id,
#             'orderpoint_id': self.id,
#             'group_id': group or self.group_id.id,
#         }


class ProcurementComputeAll(models.TransientModel):
    _name = 'procurement.order.compute.all'
    _inherit = 'procurement.order.compute.all'

    period_id = fields.Many2one('orderpoint.period', string='Orderpoint Period')

    @api.multi
    def _procure_selected_calculation(self):
        with api.Environment.manage():
            # As this function is in a new thread, i need to open a new cursor, because the old one may be closed
            new_cr = registry(self._cr.dbname).cursor()
            self = self.with_env(self.env(cr=new_cr))
            scheduler_cron = self.sudo().env.ref('procurement.ir_cron_scheduler_action')
            # Avoid to run the scheduler multiple times in the same time
            try:
                with tools.mute_logger('odoo.sql_db'):
                    self._cr.execute("SELECT id FROM ir_cron WHERE id = %s FOR UPDATE NOWAIT", (scheduler_cron.id,))
            except Exception:
                _logger.info('Attempt to run procurement scheduler aborted, as already running')
                self._cr.rollback()
                self._cr.close()
                # self.env['ir.config_parameter'].set_param('Scheduler for ' + self.period_id.name,
                #                                           'Selected Calculation Finished: ' + fields.Datetime.now())

                return {}

            Procurement = self.env['procurement.order']
            for company in self.env.user.company_ids:
                #################################################################################
                #Procurement.run_scheduler(use_new_cursor=self._cr.dbname, company_id=company.id)
                use_new_cursor = self._cr.dbname
                company_id = company.id
                ProcurementSudo = self.env['procurement.order'].sudo()
                try:
                    if use_new_cursor:
                        cr = registry(self._cr.dbname).cursor()
                        self = self.with_env(self.env(cr=cr))  # TDE FIXME

                    # Run confirmed procurements
                    procurements = ProcurementSudo.search(
                        [('state', '=', 'confirmed')] + (company_id and [('company_id', '=', company_id)] or []))
                    while procurements:
                        procurements.run(autocommit=use_new_cursor)
                        if use_new_cursor:
                            self.env.cr.commit()
                        procurements = ProcurementSudo.search(
                            [('id', 'not in', procurements.ids), ('state', '=', 'confirmed')] + (
                            company_id and [('company_id', '=', company_id)] or []))

                    # Check done procurements
                    procurements = ProcurementSudo.search(
                        [('state', '=', 'running')] + (company_id and [('company_id', '=', company_id)] or []))
                    while procurements:
                        procurements.check(autocommit=use_new_cursor)
                        if use_new_cursor:
                            self.env.cr.commit()
                        procurements = ProcurementSudo.search(
                            [('id', 'not in', procurements.ids), ('state', '=', 'running')] + (
                            company_id and [('company_id', '=', company_id)] or []))

                finally:
                    if use_new_cursor:
                        try:
                            self.env.cr.close()
                        except Exception:
                            pass

                try:
                    # Minimum stock rules
                    ########################################################################
                    # self.sudo()._procure_orderpoint_confirm(use_new_cursor=use_new_cursor,
                    #                                         company_id=company_id)
                    if use_new_cursor:
                        cr = registry(self._cr.dbname).cursor()
                        self = self.with_env(self.env(cr=cr))

                    OrderPoint = self.env['stock.warehouse.orderpoint']
                    Procurement = self.env['procurement.order']
                    ProcurementAutorundefer = Procurement.with_context(procurement_autorun_defer=True)
                    procurement_list = []

                    orderpoints_noprefetch = OrderPoint.with_context(prefetch_fields=False).search(
                        company_id and [('company_id', '=', company_id), ('period_id', '=', self.period_id.id)] or [],
                        order=Procurement._procurement_from_orderpoint_get_order())
                    while orderpoints_noprefetch:
                        orderpoints = OrderPoint.browse(orderpoints_noprefetch[:1000].ids)
                        orderpoints_noprefetch = orderpoints_noprefetch[1000:]

                        # Calculate groups that can be executed together
                        location_data = defaultdict(lambda: dict(products=self.env['product.product'],
                                                                 orderpoints=self.env['stock.warehouse.orderpoint'],
                                                                 groups=list()))
                        for orderpoint in orderpoints:
                            key = Procurement._procurement_from_orderpoint_get_grouping_key([orderpoint.id])
                            location_data[key]['products'] += orderpoint.product_id
                            location_data[key]['orderpoints'] += orderpoint
                            location_data[key]['groups'] = Procurement._procurement_from_orderpoint_get_groups([orderpoint.id])

                        for location_id, location_data in location_data.iteritems():
                            location_orderpoints = location_data['orderpoints']
                            
                            location_ids = [location_orderpoints[0].location_id.id]
                            if location_orderpoints[0].warehouse_ids:
                                location_ids = location_ids+location_orderpoints[0].warehouse_ids.ids
                            product_context = dict(self._context, location=location_ids)
                            
#                             product_context = dict(self._context, location=location_orderpoints[0].warehouse_ids.ids)
                            substract_quantity = location_orderpoints.subtract_procurements_from_orderpoints()

                            for group in location_data['groups']:
                                if group['to_date']:
                                    product_context['to_date'] = group['to_date'].strftime(
                                        DEFAULT_SERVER_DATETIME_FORMAT)
                                product_quantity = location_data['products'].with_context(
                                    product_context)._product_available()
                                
                                
                                for orderpoint in location_orderpoints:
                                    try:
                                        op_product_virtual = product_quantity[orderpoint.product_id.id][
                                            'virtual_available']
                                        if op_product_virtual is None:
                                            continue
                                        if float_compare(op_product_virtual, orderpoint.product_min_qty,
                                                         precision_rounding=orderpoint.product_uom.rounding) <= 0:
                                            qty = max(orderpoint.product_min_qty,
                                                      orderpoint.product_max_qty) - op_product_virtual
                                            remainder = orderpoint.qty_multiple > 0 and qty % orderpoint.qty_multiple or 0.0

                                            if float_compare(remainder, 0.0,
                                                             precision_rounding=orderpoint.product_uom.rounding) > 0:
                                                qty += orderpoint.qty_multiple - remainder

                                            if float_compare(qty, 0.0,
                                                             precision_rounding=orderpoint.product_uom.rounding) < 0:
                                                continue

                                            qty -= substract_quantity[orderpoint.id]
                                            qty_rounded = float_round(qty,
                                                                      precision_rounding=orderpoint.product_uom.rounding)
                                            if qty_rounded > 0:
                                                new_procurement = ProcurementAutorundefer.create(
                                                    orderpoint._prepare_procurement_values(qty_rounded, **group[
                                                        'procurement_values']))
                                                procurement_list.append(new_procurement)
                                                new_procurement.message_post_with_view('mail.message_origin_link',
                                                                                       values={'self': new_procurement,
                                                                                               'origin': orderpoint},
                                                                                       subtype_id=self.env.ref(
                                                                                           'mail.mt_note').id)
                                                Procurement._procurement_from_orderpoint_post_process([orderpoint.id])
                                            if use_new_cursor:
                                                self.env.cr.commit()

                                    except OperationalError:
                                        if use_new_cursor:
                                            orderpoints_noprefetch += orderpoint.id
                                            self.env.cr.rollback()
                                            continue
                                        else:
                                            raise

                        try:
                            # TDE CLEANME: use record set ?
                            procurement_list.reverse()
                            procurements = self.env['procurement.order']
                            for p in procurement_list:
                                procurements += p
                            procurements.run()
                            if use_new_cursor:
                                self.env.cr.commit()
                        except OperationalError:
                            if use_new_cursor:
                                self.env.cr.rollback()
                                continue
                            else:
                                raise

                        if use_new_cursor:
                            self.env.cr.commit()

                    # if use_new_cursor:
                    #     self.env.cr.commit()
                    #     try:
                    #         self.env.cr.close()
                    #     except Exception:
                    #         pass



                    # Search all confirmed stock_moves and try to assign them
                    confirmed_moves = self.env['stock.move'].search([('state', '=', 'confirmed')], limit=None,
                                                                    order='priority desc, date_expected asc')
                    for x in xrange(0, len(confirmed_moves.ids), 100):
                        # TDE CLEANME: muf muf
                        self.env['stock.move'].browse(confirmed_moves.ids[x:x + 100]).action_assign()
                        if use_new_cursor:
                            self._cr.commit()

                    if use_new_cursor:
                        self.env.cr.commit()
                        try:
                            self.env.cr.close()
                        except Exception:
                            pass
                finally:
                    if use_new_cursor:
                        try:
                            self.env.cr.close()
                        except Exception:
                            pass


            # close the new cursor
            try:
                self._cr.close()
            except Exception:
                pass
            try:
                self.env.cr.close()
            except Exception:
                pass
            # self.env['ir.config_parameter'].set_param('Scheduler for ' + self.period_id.name,
            #                                           'Selected Calculation Finished: ' + fields.Datetime.now())
            return {}

    @api.multi
    def procure_selected_calculation(self):
        # self.env['ir.config_parameter'].set_param('Scheduler for ' + self.period_id.name, 'Selected Calculation Started: ' + fields.Datetime.now())
        threaded_calculation = threading.Thread(target=self._procure_selected_calculation, args=())
        threaded_calculation.start()
        return {'type': 'ir.actions.act_window_close'}

    @api.multi
    def procure_calculation(self):
        # self.env['ir.config_parameter'].set_param('Scheduler',
        #                                           'Calculation Started: ' + fields.Datetime.now())
        return super(ProcurementComputeAll,self).procure_calculation()

    @api.multi
    def _procure_calculation_all(self):
        res = super(ProcurementComputeAll,self)._procure_calculation_all()
        # self.env['ir.config_parameter'].set_param('Scheduler',
        #                                           'Calculation Finished: ' + fields.Datetime.now())
        return res


class ProcurementOrder(models.Model):
    _inherit = "procurement.order"


    @api.multi
    @api.returns('procurement.rule', lambda value: value.id if value else False)
    def _find_suitable_rule(self):
        rule = super(ProcurementOrder, self)._find_suitable_rule()
        if not rule:
            # a rule defined on 'Stock' is suitable for a procurement in 'Stock\Bin A'
            all_parent_location_ids = self._find_parent_locations()
            rule = self._search_suitable_rule([('location_id', 'in', all_parent_location_ids.ids)])
        return rule
    
    def _search_suitable_rule(self, domain):
        """ First find a rule among the ones defined on the procurement order
        group; then try on the routes defined for the product; finally fallback
        on the default behavior """
        if self.warehouse_id:
            domain = expression.AND([['|', ('warehouse_id', '=', self.warehouse_id.id), ('warehouse_id', '=', False)], domain])
        Pull = self.env['procurement.rule']
        res = self.env['procurement.rule']
        if self.route_ids:
            res = Pull.search(expression.AND([[('route_id', 'in', self.route_ids.ids)], domain]), order='route_sequence, sequence', limit=1)
        if not res:
            
            product_routes = self.product_id.route_ids | self.product_id.categ_id.total_route_ids
            if product_routes:
                res = Pull.search(expression.AND([[('route_id', 'in', product_routes.ids)], domain]), order='route_sequence, sequence', limit=1)
        if not res:
            warehouse_routes = self.warehouse_id.route_ids
            if warehouse_routes:
                res = Pull.search(expression.AND([[('route_id', 'in', warehouse_routes.ids)], domain]), order='route_sequence, sequence', limit=1)
        if not res:
            res = Pull.search(expression.AND([[('route_id', '=', False)], domain]), order='sequence', limit=1)
        return res


    @api.multi
    def _run(self):
        if self.rule_id.action == 'move':
            if not self.rule_id.location_src_id:
                self.message_post(body=_('No source location defined!'))
                return False
            # create the move as SUPERUSER because the current user may not have the rights to do it (mto product launched by a sale for example)
            self.env['stock.move'].sudo().create(self._get_stock_move_values())
            return True
        return super(ProcurementOrder, self)._run()

    @api.multi
    def run(self, autocommit=False):
        # TDE CLEANME: unused context key procurement_auto_defer remove
        new_self = self.filtered(lambda order: order.state not in ['running', 'done', 'cancel'])
        res = super(ProcurementOrder, new_self).run(autocommit=autocommit)

        # after all the procurements are run, check if some created a draft stock move that needs to be confirmed
        # (we do that in batch because it fasts the picking assignation and the picking state computation)
        new_self.filtered(lambda order: order.state == 'running' and order.rule_id.action == 'move').mapped('move_ids').filtered(lambda move: move.state == 'draft').action_confirm()

        # TDE FIXME: action_confirm in stock_move already call run() ... necessary ??
        # If procurements created other procurements, run the created in batch
        new_procurements = self.search([('move_dest_id.procurement_id', 'in', new_self.ids)], order='id')
        if new_procurements:
            res = new_procurements.run(autocommit=autocommit)
        return res

    @api.multi
    def _check(self):
        """ Checking rules of type 'move': satisfied only if all related moves
        are done/cancel and if the requested quantity is moved. """
        if self.rule_id.action == 'move':
            # In case Phantom BoM splits only into procurements
            if not self.move_ids:
                return True
            move_all_done_or_cancel = all(move.state in ['done', 'cancel'] for move in self.move_ids)
            move_all_cancel = all(move.state == 'cancel' for move in self.move_ids)
            if not move_all_done_or_cancel:
                return False
            elif move_all_done_or_cancel and not move_all_cancel:
                return True
            else:
                self.message_post(body=_('All stock moves have been cancelled for this procurement.'))
                # TDE FIXME: strange that a check method actually modified the procurement...
                self.write({'state': 'cancel'})
                return False
        return super(ProcurementOrder, self)._check()
