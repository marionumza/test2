# -*- coding: utf-8 -*-
# Part of Softhealer Technologies.
{
    "name" : "Website FAQ",
    "author" : "Softhealer Technologies",
    "website": "https://www.softhealer.com",
    "category": "Website",
    "summary": "This module useful to show Frequently Asked Question list by category.",
    "description": """
    
Website F.A.Q. is a complete Frequently Asked Question Management module for odoo. It provides an F.A.Q. With category which can be used to effortlessly add F.A.Q. sections to your website.
Organizing the most common support requests you get into a F.A.Q. section, will save you time, by drastically reducing the number of incoming support emails!
F.A.Q. offers you unlimited categories to organize your content! And off course, you can have unlimited sets of Questions and Answers in each category! All types of content text-images-videos-script are allowed in each answer through the editor! So, become creative, and enhance your customers experience with rich content, images and even some video tutorials!
    
                    """,    
    "version":"10.0.1",
    "depends" : ["base","website","web"],
    "application" : True,
    "data" : ['security/ir.model.access.csv',
              'views/faq_view.xml',
              'data/faq_data.xml',
              'views/faq_template.xml',
            ],           
    "images": ["static/description/background.png",],              
    "auto_install":False,
    "installable" : True,
    "price": 18,
    "currency": "EUR"   
}
