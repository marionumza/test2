# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import json
import logging
from werkzeug.exceptions import Forbidden

from odoo import http, tools, _
from odoo.http import request
from odoo.addons.base.ir.ir_qweb.fields import nl2br
from odoo.addons.website.models.website import slug
from odoo.addons.website.controllers.main import QueryURL
from odoo.exceptions import ValidationError
from odoo.addons.website_form.controllers.main import WebsiteForm
from odoo.addons.website.controllers.main import Home
from odoo.addons.website_sale.controllers.main import WebsiteSale
from odoo.addons.web.controllers.main import set_cookie_and_redirect
from odoo.addons.auth_signup.controllers.main import AuthSignupHome


# from addons.web.controllers.main import Session
import werkzeug.utils
import werkzeug.wrappers

_logger = logging.getLogger(__name__)

class CustomTheme(http.Controller):
    
    @http.route(['/shop/wishlist/add'], type='json', auth="user", website=True)
    def add_to_wishlist(self, product_id, price=False, **kw):
        if not price:
            
            pricelist_context = dict(request.env.context)
            if not pricelist_context.get('pricelist'):
                pricelist = request.website.get_current_pricelist()
                pricelist_context['pricelist'] = pricelist.id
            else:
                pricelist = request.env['product.pricelist'].sudo().browse(pricelist_context['pricelist'])

            from_currency = request.env.user.company_id.currency_id
            to_currency = pricelist.currency_id
            compute_currency = lambda price: from_currency.compute(price, to_currency)
            
            p = request.env['product.product'].with_context(pricelist_context, display_default_code=False).browse(product_id)
            price = p.website_price
        vals = request.env['product.wishlist']._add_to_wishlist(
            request.env.user.partner_id.id,
            pricelist.id,
            pricelist.currency_id.id,
            request.website.id,
            price,
            product_id,
            request.uid,
        )
        return True
    @http.route('/wishlist/user', type='json', auth="public", website=True)
    def check_login_user(self, **post):
        if request.session.uid:
            return True
        else:
            return False
    


    @http.route(['/shop/wishlist'], type='http', auth="public", website=True)
    def get_wishlist(self, count=False, **kw):
        validate_wishlist = self.check_login_user()
        if validate_wishlist:
            values = request.env['product.wishlist'].with_context(display_default_code=False).sudo().search([('partner_id', '=', request.env.user.partner_id.id)])
            if count:
                return request.make_response(json.dumps(values.mapped('product_id').ids))

            # if not len(values):
            #   return request.redirect("/shop")

            return request.render("Shoeembassy_Hero.product_wishlist", dict(wishes=values))
        else:
            return request.redirect('/web/login')

    
    @http.route(['/Shoeembassy_Hero/mobile_preview/object'], type='json', auth="user", website=True)
    def input_data_processing(self,product_id, **kw):
        product_context = dict(request.env.context)
        product_obj= request.env['product.template'].search_read([('id', '=',product_id)])
        return product_obj
       

      


