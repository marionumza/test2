odoo.define('website.mobile', function (require) {
'use strict';

var ajax = require('web.ajax');
var core = require('web.core');
var Dialog = require('web.Dialog');
var Widget = require('web.Widget');
var base = require('web_editor.base');
var website = require('website.website');
var Model = require('web.Model');
var session = require('web.session');
var QWeb = core.qweb;
var _t = core._t;
// var slideIndex = 1;
// showSlides(slideIndex);


var MobilePreviewDialog = Dialog.extend({
    template: 'website.mobile_preview',
    // events: {
    //     'click .prev': 'plusSlides',
    //     'click .next': 'plusSlides',
    //     'click .dot': 'currentSlide',
        
    // },

    init: function () {
        this._super.apply(this, arguments);
        this.mobile_src = $.param.querystring(window.location.href, 'mobilepreview');
    },

    start: function () {
        var self = this;
        this.$modal.addClass('oe_mobile_preview');
        this.$modal.on('click', '.modal-header', function () {
            self.$el.toggleClass('o_invert_orientation');
           
        });   
        this.$iframe = this.$('iframe');       
        var url= $.param.querystring(window.location.href, 'mobilepreview');
        var newString = url.split("/product").pop();
        var stringSlipt=newString.slice(0, newString.indexOf("?"));
        var product_id=stringSlipt.split('-').pop();        
        var product_record = []; 
        ajax.jsonRpc("/Shoeembassy_Hero/mobile_preview/object", 'call', {
                            
                           'product_id': product_id,

                     }).then(function (data) {product_record.push(data);
                       
                        
        // this.$iframe = this.$('iframe');
        console.log("this",self.$iframe);
        

        
        self.$iframe.on('load', function (e) {
    
            // passing here when mobile icon is clicked
            //removig div for mobile view
            console.log("data",data);
            self.$iframe.contents().find('body').removeClass('o_connected_user');
            self.$iframe.contents().find('#oe_main_menu_navbar, #o_website_add_page_modal,#ex1').remove();
            self.$iframe.contents().find('.c_product_img').css({"display":"none"});
            self.$iframe.contents().find('#mobile_carousel').add();
            self.$iframe.contents().find('.slideshow-container').css({"display":"block"});
            
            
             }); 
                        
         
         });
        
        return this._super.apply(this, arguments);
    },

    // showSlides: function (event,n) {
            
    //           console.log("pass--3",slideIndex);
    //           var i;
    //           var slides = document.getElementsByClassName("mySlides");
    //           var dots = document.getElementsByClassName("dot");
    //           if (n > slides.length) {slideIndex = 1}    
    //           if (n < 1) {slideIndex = slides.length}
    //           for (i = 0; i < slides.length; i++) {
    //               slides[i].style.display = "none";  
    //           }
    //           for (i = 0; i < dots.length; i++) {
    //               dots[i].className = dots[i].className.replace("active", "");
    //           }
    //           slides[slideIndex-1].style.display = "block";  
    //           dots[slideIndex-1].className += "active";
    //         }, 

    
            
    // currentSlide: function (event,n) {
            
    //                 console.log("pass--1");
    //                 showSlides(slideIndex = n);
    //                 },
    // plusSlides: function (event,n){
            
    //                console.log("pass--2");
    //                showSlides(slideIndex += n);
    //             },
    
});

website.TopBar.include({
    start: function () {
        var self = this;
        this.$el.on('click', 'a[data-action=show-mobile-preview]', function () {           
            new MobilePreviewDialog(self, {               
                title: _t('Mobile preview') + " <span class='fa fa-refresh'/>",
            }).open();        
            
        });  
        
        return this._super();
    },
});
});


