# -*- coding: utf-8 -*-
from odoo import api, fields, models, _

class warehouse(models.Model):

	_inherit = 'stock.warehouse'

	image = fields.Binary(string='Image', attachment=True)
	image_medium = fields.Binary("Medium-sized image", attachment=True)
	opening_hours_start_days = fields.Selection([('monday', 'Monday'), ('Tuesday', 'Tuesday'), 
		('wensday', 'Wensday'), ('thursday', 'Thursday'),
		('friday', 'Friday'), ('saturday', 'Saturday')], string="Start Day")
	opening_hours_end_days = fields.Selection([('monday', 'Monday'), ('Tuesday', 'Tuesday'), 
		('wensday', 'Wensday'), ('thursday', 'Thursday'),
		('friday', 'Friday'), ('saturday', 'Saturday')], string="End Day")
	start_time = fields.Char(string="Start Time")
	end_time = fields.Char(string="End Time")
	opening_hours_for_sunday = fields.Char(string= 'Opening Hours For Sunday')
	start_time_sunday = fields.Char(string="Start Time For Sunday")
	end_time_sunday = fields.Char(string="End Time For Sunday")
	is_active = fields.Boolean('Active')