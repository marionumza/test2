# -*- coding: utf-8 -*-



from odoo import models, fields, osv, api
from odoo.exceptions import ValidationError
from datetime import datetime, date
import os

from odoo.tools.translate import _
from odoo.osv import osv
import json
import ast

import csv
import base64


class ShoeEmbassyOrdersImport2Vend(models.Model):
    _name = "shoe.embassy.orders.import2.vend"

    name = fields.Char('Note')
    file = fields.Binary('Import Vend Orders File')
    filename = fields.Char()


    def shoe_embassy_orders_import_from_csv(self):

        fo = open('/tmp/shoe_embassy_vend_orders.csv', 'wb+')
        fo.write(base64.b64decode(self.file))
        fo.close()
        f = open('/tmp/shoe_embassy_vend_orders.csv')
        csv_f = csv.reader(f)
        cont = 1
        headers = []

        number_next_actual = self.env['ir.sequence'].search([('code', '=', 'pos.session')]).number_next_actual
        for row in csv_f:
            print cont
            if cont == 1:
                cont += 1
                for r in row:
                    headers.append(r)
                continue
            else:
                cell_count = 0
                order_vals = {}
                order_line_vals = {}
                partner_vals = {}

                # Date
                order_vals.update({'date_order': unicode(row[cell_count])})
                cell_count += 1

                # Receipt Number
                order_vals.update({'name': row[cell_count].decode('utf-8')})
                order_vals.update({'pos_reference': row[cell_count].decode('utf-8')})
                cell_count += 1

                # Line Type
                line_type = row[cell_count].decode('utf-8')
                cell_count += 1

                # Customer Code
                cell_count += 1

                # Customer Name
                cell_count += 1

                # Note
                order_vals.update({'note': row[cell_count].decode('utf-8')})
                cell_count += 1

                # Quantity
                if row[cell_count]:
                    qty = int(row[cell_count])
                    order_line_vals.update({'qty': abs(int(row[cell_count]))})
                cell_count += 1

                # Subtotal
                cell_count += 1

                # Sales Tax
                if line_type == 'Sale Line':
                    tax = self.env['account.tax'].search([
                        #('amount', '=', row[cell_count]),
                        ('name', '=', 'Standard rate sales (20%)'),
                        ('type_tax_use', '=', 'sale')])
                    # if len(tax) > 1:
                    #     tax = tax[0] and not tax.id
                    # if row[cell_count]:
                    #     # tax = self.env['account.tax'].create({
                        #     'amount': unicode(row[cell_count]),
                        #     'name': unicode(row[cell_count]),
                        #     'type_tax_use': 'sale'})
                    order_line_vals.update({'tax_ids': [(6, 0, [13])]})
                cell_count += 1

                # Discount 	discount
                if row[cell_count]:
                    discount = float(row[cell_count])
                else:
                    discount = 0
                cell_count += 1

                # Total
                if line_type != "Sale":
                    if row[cell_count]:
                        unit = float(row[cell_count])
                        if unit != 0 and (unit + discount) != 0:
                            order_line_vals.update({
                                'price_unit': unit + discount * qty})
                            order_line_vals.update({'discount': (discount * qty / float(order_line_vals['price_unit']) * 100)})
                        else:
                            order_line_vals.update({
                                'price_unit': unit,
                                'discount': 0})
                cell_count += 1

                # Paid
                amount = row[cell_count]
                cell_count += 1

                # Details
                details = row[cell_count].decode('utf-8')
                cell_count += 1

                # Register
                if line_type == 'Sale':
                    warehouse = self.env['stock.warehouse'].search([('name', '=', unicode(row[cell_count]))])
                    if len(warehouse) == 0:
                        raise ValueError(_(str(cont) + " - Warehouse " + unicode(row[cell_count]) + " is not in the system"))
                    order_vals.update({'location_id': warehouse.lot_stock_id.id or False})
                cell_count += 1

                # User
                partner_vals.update({'name': row[cell_count].decode('utf-8')})
                cell_count += 1

                # Status
                cell_count += 1

                # Sku
                if line_type == 'Sale Line':
                    if details == 'Gift Card':
                        product = self.env['product.product'].search([('name', '=', 'Gift Card Product')])
                        if len(product) == 0:
                            raise ValueError(_("There is no product in the system with name Gift Card Product"))
                        order_line_vals.update({'product_id': product.id or False})
                        cell_count += 1
                    elif details == 'Discount':
                        product = self.env['product.product'].search([('name', '=', 'Discount')])
                        if len(product) == 0:
                            raise ValueError(_("There is no product in the system with name Discount"))
                        order_line_vals.update({'product_id': product.id or False})
                        cell_count += 1
                    else:
                        product = self.env['product.product'].search([('barcode', '=', unicode(row[cell_count]))])
                        if len(product) == 0:
                            raise ValueError(_("There is no product in the system with the barcode " + unicode(row[cell_count])))
                        order_line_vals.update({'product_id': product.id or False})
                        cell_count += 1
                else:
                    print details

                # AccountCodeSale
                cell_count += 1

                # AccountCodePurchase
                cell_count += 1





                if line_type == 'Sale':
                    # Salesman block
                    user = self.env['res.users'].search([('name', '=', warehouse.name + ' POS')])
                    if len(user) == 0:
                        user_vals = {
                            'name': warehouse.name + ' POS',
                            'login': warehouse.name + '@shoeembassy.com',
                        }
                        user = self.env['res.users'].create(user_vals)

                    partner = self.env['res.partner'].search(['|',
                        ('name', '=', partner_vals['name']),
                        ('email', '=',partner_vals['name']),
                        ('parent_id', '=', False)
                    ])
                    if len(partner) == 0:
                        partner_vals.update({'user_id': user.id})
                        partner = self.env['res.partner'].create(partner_vals)

                    if len(partner.user_id) == 0:
                        partner.write({
                            'user_id': user.id,
                        })

                    # session block
                    pos_config = self.env['pos.config'].search([
                        ('stock_location_id', '=', warehouse.lot_stock_id.id)])
                    if not pos_config:
                        raise ValidationError(_('Consider to setup POS CONFIG before import for the ' + warehouse.name))

                    session = self.env['pos.session'].search([
                        ('user_id', '=', user.id),
                        ('config_id', '=', pos_config.id),
                        ('start_at', '=', order_vals['date_order'] + ' 00:00:00')

                    ])
                    if not session:
                        seq = self.env['pos.session'].search([('start_at', '=', order_vals['date_order'] + ' 00:00:00')])
                        session = self.env['pos.session'].create({
                            'user_id': user.id,
                            'config_id': pos_config.id,
                            'name': 'Imported',
                            'state': 'closed',
                            'start_at': order_vals['date_order'] + ' 00:00:00',
                            'stop_at': order_vals['date_order'] + ' 23:59:59'})

                        session.write({'name': "POS/" + order_vals['date_order'] + "/" + str(len(seq) + 1) + ' Imported'})

                    order_vals.update({
                        'sperson_id': partner.id or False,
                        'user_id': user.id,
                        'pricelist_id': 1,
                        'session_id': session.id,
                        'state': 'done',
                         })

                    # Order Block
                    order = self.env['pos.order'].search([
                        ('pos_reference', '=', order_vals['pos_reference']),
                        ('location_id', '=', warehouse.lot_stock_id.id)])
                    if len(order) == 0:
                        order = self.env['pos.order'].create(order_vals)
                    else:
                        order_vals.pop("name")
                        order.write(order_vals)

                    previous_order_line = None

                elif line_type == 'Sale Line' :# and details != 'Discount'
                    order = self.env['pos.order'].search([
                        ('pos_reference', '=', order_vals['pos_reference']),
                        ('location_id', '=', warehouse.lot_stock_id.id)])
                    order_line_vals.update({'order_id': order.id})
                    order_line = self.env['pos.order.line'].search([
                        ('order_id', '=', order.id),
                        ('product_id', '=', product.id),
                        ('price_unit', '=', order_line_vals['price_unit'])
                        ])
                    if len(order_line) == 0:
                        order_line = self.env['pos.order.line'].create(order_line_vals)
                    else:
                        order_line.write(order_line_vals)
                # elif line_type == 'Sale Line' and details == 'Discount':
                #     discount_values = {}
                else:
                    order = self.env['pos.order'].search([
                        ('pos_reference', '=', order_vals['pos_reference']),
                        ('location_id', '=', warehouse.lot_stock_id.id)])
                    jornal = self.env['account.journal'].search([('name', '=', details + ' ' + warehouse.name)])
                    if len(jornal) == 0:
                        raise ValueError(_('"' + details + ' ' + warehouse.name + '" journal is not in the system'))
                    statement = self.env['account.bank.statement'].search([
                        ('journal_id', '=', jornal.id),
                        ('pos_session_id', '=', order.session_id.id)])
                    warehouse = self.env['stock.warehouse'].search([('lot_stock_id', '=', order.location_id.id)])
                    statement_line = self.env['account.bank.statement.line'].search([
                        ('date', '=', order.date_order),('amount', '=', amount,),
                        ('name', '=', warehouse.name + '/' + order_vals['name']),
                        ('statement_id', '=', statement.id)])
                    if len(statement_line) == 0:
                        if float(amount) != 0:
                            statement_line = self.env['account.bank.statement.line'].create({
                                'name': warehouse.name + '/' + order_vals['name'],
                                'date': order.date_order,
                                'statement_id': statement.id,
                                'ref': warehouse.name + '/' + order.name,
                                'amount': amount,
                                'pos_statement_id': order.id,
                                #'journal_id': jornal.id,
                            })
                    else:
                        if float(amount) != 0:
                            statement_line.write({
                                'name': warehouse.name + '/' + order_vals['name'],
                                'amount': amount,
                                'journal_id': jornal.id,
                            })
            # Change create date
            self._cr.execute(""" UPDATE pos_order SET create_date='%s'
                                 WHERE id=%d"""
                             % (order_vals['date_order'], order.id))
            cont += 1

        self.env['ir.sequence'].search([('code', '=', 'pos.session')]).write({
            'number_next_actual': number_next_actual})
        os.remove('/tmp/shoe_embassy_vend_orders.csv')
        return True



class AccountBankStatement(models.Model):

    _name = "account.bank.statement"
    _inherit = "account.bank.statement"
    
    @api.model
    def create(self, vals):
        return super(AccountBankStatement,self).create(vals)