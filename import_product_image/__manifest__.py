# -*- coding: utf-8 -*-

{
    "name": "Import product Image from link",
    "summary": "",
    "version": "1.0.0",
    "category": "Inventory",
    "author": "Anywhereconnex",
    "license": "AGPL-3",
    "application": True,
    "installable": True,
    "images": [],
    "depends": [
        "product",
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/product_import_image_view.xml'
    ],
    'currency': 'EUR'
}
