# -*- coding: utf-8 -*-


from odoo import models, fields, osv, api
from odoo.exceptions import ValidationError
from datetime import datetime, date
import os


from odoo.tools.translate import _
from odoo.osv import osv
import json
import ast

import csv
import base64

class Shoe_Embassy_Barcode_import(models.Model):
    _name = "shoe.embassy.barcode.import"

    name = fields.Char('Note')
    file = fields.Binary('Import Barcode File')
    filename = fields.Char()

    

    def shoe_embassy_barcode_import_from_csv(self):

        fo = open('/tmp/shoe_embassy_barcode.csv', 'wb+')
        fo.write(base64.b64decode(self.file))
        fo.close()
        f = open('/tmp/shoe_embassy_barcode.csv')
        csv_f = csv.reader(f)
        cont = 1
        headers = []
        for row in csv_f:
            print cont
            if cont == 1:
                cont += 1
                for r in row:
                    headers.append(r)
                continue
            else:
                cell_count = 0
                message = ''

                # Barcode
                barcode_value = unicode(row[cell_count])
                barcode = self.env['shoe.embassy.barcode'].search([('name', '=', barcode_value)])
                if len(barcode) == 0:
                    barcode = self.env['shoe.embassy.barcode'].create({'name': barcode_value})


                product = self.env['product.product'].search([('barcode', '=', barcode_value)])
                if len(product) == 1:
                    barcode.write({'product_id': product.id})
                elif len(product) > 1:
                    message += 'Barcode ' + barcode_value + ' is used more then 1 time\n'

            cont += 1

        os.remove('/tmp/shoe_embassy_barcode.csv')
        self.write({'name': message})
        return True


