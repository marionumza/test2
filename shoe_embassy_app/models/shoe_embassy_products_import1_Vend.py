# -*- coding: utf-8 -*-


from odoo import models, fields, osv, api
from odoo.exceptions import ValidationError
from datetime import datetime, date
import os


from odoo.tools.translate import _
from odoo.osv import osv
import json
import ast

import csv
import base64

class ShoeEmbassyProductImport1Vend(models.Model):
    _name = "shoe.embassy.product.import1.vend"

    name = fields.Char('Note')
    file = fields.Binary('Import Vend File')
    filename = fields.Char()

    def shoe_embassy_product_import_from_csv(self):

        fo = open('/tmp/shoe_embassy_product1.csv', 'wb+')
        fo.write(base64.b64decode(self.file))
        fo.close()
        f = open('/tmp/shoe_embassy_product1.csv')
        csv_f = csv.reader(f)
        cont = 1
        headers = []
        for row in csv_f:
            if cont == 1:
                cont += 1
                for r in row:
                    headers.append(r)
                continue
            else:
                cell_count = 0
                product_vals = {}
                variant_vals = {}
                variant_colour_vals = {}
                product_product_vals = {}
                product_size_vals = {}
                product_colour_vals = {}

                # id
                cell_count += 1

                # handle
                product_vals.update({'name': row[cell_count].decode('utf-8')})
                print str(cont) + ' ' + row[cell_count].decode('utf-8')
                # handle = self.env['shoe.embassy.handle'].search([('name', '=', row[cell_count])])
                # if row[cell_count] and not handle.id:
                #     handle = self.env['shoe.embassy.handle'].create({'name': row[cell_count].decode('utf-8')})
                #product_vals.update({'handle_id': handle.id or False})
                cell_count += 1

                # Product Code
                product_product_vals.update({'default_code': unicode(row[cell_count])})
                product_product_vals.update({'barcode': unicode(row[cell_count])})
                cell_count += 1

                # composite_handle
                cell_count += 1

                # composite_sku
                cell_count += 1

                # composite_quantity
                cell_count += 1

                # name
                product_vals.update({'description_purchase': row[cell_count].decode('utf-8')})
                #product_product_vals.update({'name': row[cell_count].decode('utf-8')})
                #product_vals.update({'default_code': unicode(row[cell_count])})
                cell_count += 1

                # description
                product_vals.update({'description': row[cell_count].decode('utf-8')})
                cell_count += 1

                # type
                shoe_type = self.env['shoe.embassy.shoe_type'].search([('name', '=', row[cell_count])])
                if row[cell_count] and not shoe_type.id:
                    shoe_type = self.env['shoe.embassy.shoe_type'].create({'name': row[cell_count].decode('utf-8')})
                product_vals.update({"shoe_type_id": shoe_type.id or False})
                cell_count += 1

                # variant_option_one_name
                variant_vals.update({'attribute_name': row[cell_count].decode('utf-8')})
                cell_count += 1

                # variant_option_one_value
                variant_vals.update({'value': unicode(row[cell_count])})
                variant = unicode(row[cell_count])
                cell_count += 1

                # variant_option_two_name
                cell_count += 1

                # variant_option_two_value
                cell_count += 1

                # variant_option_three_name
                cell_count += 1

                # variant_option_three_value
                cell_count += 1

                # tags
                cell_count += 1

                # supply_price
                #product_vals.update({'list_price': unicode(row[cell_count])})
                cell_count += 1

                # retail_price
                product_vals.update({'list_price': unicode(row[cell_count])})
                cell_count += 1

                # tax_name
                cell_count += 1

                # tax_value
                # tax = self.env['account.tax'].search([('amount', '=', row[cell_count])])
                # if row[cell_count] and not tax.id:
                #     tax = self.env['account.tax'].create({'amount': unicode(row[cell_count]),'name': unicode(row[cell_count-1]) + '-' + row[cell_count].decode('utf-8')})
                # product_vals.update({'taxes_id': [tax.id or False]})
                cell_count += 1

                # account_code
                cell_count += 1

                # account_code_purchase
                cell_count += 1

                # brand_name
                # brand = self.env['shoe.embassy.brand'].search([('name', '=', row[cell_count])])
                # if row[cell_count] and not brand.id:
                #     brand = self.env['shoe.embassy.brand'].create({'name': row[cell_count].decode('utf-8')})
                # product_vals.update({'brand_id': brand.id or False})
                cell_count += 1

                # supplier_name
                #product.supplierinfo
                # supplier = self.env['res.partner'].search([('name', '=', row[cell_count])])
                # if row[cell_count] and not supplier.id:
                #     supplier = self.env['res.partner'].create({'name': row[cell_count].decode('utf-8')})
                # product_vals.update({"name": supplier.id or False})
                cell_count += 1

                # supplier_code
                #product_vals.update({'default_code': unicode(row[cell_count])})
                cell_count += 1

                # active
                cell_count += 1

                # track_inventory
                cell_count += 1

                #inventory_Bluewater
                #inventory_vals.update({'Bluewater': {'inventory': row[cell_count], 'reorder': row[cell_count + 1], 'restock': row[cell_count + 2]}})
                cell_count += 1

                # reorder_point_Bluewater
                cell_count += 1

                # restock_level_Bluewater
                cell_count += 1

                # inventory_Brick_Lane_Sunday_Up_Market
                #inventory_vals.update({'Brick_Lane_Sunday_Up_Market': {'inventory': row[cell_count], 'reorder': row[cell_count + 1], 'restock': row[cell_count + 2]}})
                cell_count += 1

                # reorder_point_Brick_Lane_Sunday_Up_Market
                cell_count += 1

                # restock_level_Brick_Lane_Sunday_Up_Market
                cell_count += 4

                # inventory_Camden_Stables_Market
                #inventory_vals.update({'Camden_Stables_Market': {'inventory': row[cell_count], 'reorder': row[cell_count + 1],'restock': row[cell_count + 2]}})
                cell_count += 1

                # reorder_point_Camden_Stables_Market
                cell_count += 1

                # restock_level_Camden_Stables_Market
                cell_count += 1

                # inventory_Greenwich_Market
                #inventory_vals.update({'Greenwich_Market': {'inventory': row[cell_count], 'reorder': row[cell_count + 1], 'restock': row[cell_count + 2]}})
                cell_count += 1

                # reorder_point_Greenwich_Market
                cell_count += 1

                # restock_level_Greenwich_Market
                cell_count += 1

                # inventory_Old_Spitalfields_Market
                #inventory_vals.update({'Old_Spitalfields_Market': {'inventory': row[cell_count], 'reorder': row[cell_count + 1], 'restock': row[cell_count + 2]}})
                cell_count += 1

                # reorder_point_Old_Spitalfields_Market
                cell_count += 1

                # restock_level_Old_Spitalfields_Market
                cell_count += 1

                # inventory_Oxford
                #inventory_vals.update({'Oxford': {'inventory': row[cell_count], 'reorder': row[cell_count + 1], 'restock': row[cell_count + 2]}})
                cell_count += 1

                # reorder_point_Oxford
                cell_count += 1

                # restock_level_Oxford
                cell_count += 1

                # inventory_Warehouse
                #inventory_vals.update({'Warehouse': {'inventory': row[cell_count], 'reorder': row[cell_count + 1], 'restock': row[cell_count + 2]}})
                cell_count += 1

                # reorder_point_Warehouse
                cell_count += 1

                # restock_level_Warehouse
                cell_count += 1

                # inventory_Westfield
                #inventory_vals.update({'Westfield': {'inventory': row[cell_count], 'reorder': row[cell_count + 1], 'restock': row[cell_count + 2]}})
                cell_count += 1

                # reorder_point_Westfield
                cell_count += 1

                # restock_level_Westfield
                cell_count += 1

                # Model
                model = self.env['shoe.embassy.model'].search([('name', '=', row[cell_count])])
                if row[cell_count] and not model.id:
                    model = self.env['shoe.embassy.model'].create({'name': row[cell_count].decode('utf-8')})
                product_vals.update({'model_id': model.id or False})
                cell_count += 1

                # Collection
                # collection = self.env['shoe.embassy.collection'].search([('name', '=', row[cell_count])])
                # if row[cell_count] and not collection.id:
                #     collection = self.env['shoe.embassy.collection'].create({'name': row[cell_count].decode('utf-8')})
                # product_vals.update({'collection_id': collection.id or False})
                cell_count += 1

                # Season
                # if unicode(row[cell_count]) not in ['Summer', 'Spr-Aut', 'Winter', 'Mix', '']:
                #     raise ValidationError(_('Error on line ' + str(cont) + '. Cell ' + str(cell_count) + ' ' +
                #                             headers[cell_count] + ' must be Summer/Spr-Aut/Winter/Mix.'))
                # product_vals.update({'season': row[cell_count].decode('utf-8')})
                cell_count += 1

                # Gender
                if unicode(row[cell_count]) not in ['Womens','Mens', '']:
                    raise ValidationError(_('Error on line ' + str(cont) + '. Cell ' + str(cell_count) + ' ' +
                                            headers[cell_count] + ' must be Womens/Mens.'))
                product_vals.update({'gender': unicode(row[cell_count])})
                cell_count += 1

                # Colour Name
                colour = self.env['shoe.embassy.colour'].search([('name', '=', row[cell_count])])
                if row[cell_count] and not colour.id:
                    colour = self.env['shoe.embassy.colour'].create({'name': row[cell_count].decode('utf-8')})
                product_vals.update({'colour_id': colour.id or False})
                product_vals.update({'description_sale': product_vals['name'] + '/' + row[cell_count].decode('utf-8')})
                # variant_colour_vals.update({'attribute_name': 'Colour'})
                # variant_colour_vals.update({'value': unicode(row[cell_count])})
                cell_count += 1

                # Colour Code
                colour_code = self.env['shoe.embassy.colour.code'].search([('name', '=', row[cell_count])])
                if row[cell_count] and not colour_code.id:
                    colour_code = self.env['shoe.embassy.colour.code'].create({'name': row[cell_count].decode('utf-8'), 'colour_id': colour.id or False})
                product_vals.update({'colour_code_id': colour_code.id or False})
                cell_count += 1


                # attribute block SIZE
                attribute_size = self.env['product.attribute'].search(
                    [('name', '=', variant_vals['attribute_name'])])
                if variant_vals['attribute_name'] and len(attribute_size) == 0:
                    attribute_size = self.env['product.attribute'].create({'name': variant_vals['attribute_name']})

                value_size = self.env['product.attribute.value'].search([
                    ('name', '=', variant_vals['value']),
                    ('attribute_id', '=', attribute_size.id)])
                if variant_vals['value'] and len(value_size) == 0:
                    value_size = self.env['product.attribute.value'].create({
                        'name': variant_vals['value'],
                        'attribute_id': attribute_size.id})

                # # attribute block COLOUR
                # attribute_colour = self.env['product.attribute'].search(
                #     [('name', '=', variant_colour_vals['attribute_name'])])
                # if variant_colour_vals['attribute_name'] and len(attribute_colour) == 0:
                #     attribute_colour = self.env['product.attribute'].create({'name': variant_colour_vals['attribute_name']})
                #
                # value_colour = self.env['product.attribute.value'].search([
                #     ('name', '=', variant_colour_vals['value']),
                #     ('attribute_id', '=', attribute_colour.id)])
                # if variant_colour_vals['value'] and len(value_colour) == 0:
                #     value_colour = self.env['product.attribute.value'].create({
                #         'name': variant_colour_vals['value'],
                #         'attribute_id': attribute_colour.id})



                # PRODUCT creation block
                product_vals.update({'type': 'product'})
                #product = self.env['product.template'].search([('default_code', '=', product_vals['default_code'])])
                product = self.env['product.template'].search([('name', '=', product_vals['name'])])
                if len(product) == 0:
                    # try:
                    product = self.env['product.template'].create(product_vals)
                    product_product = self.env['product.product'].search([('product_tmpl_id', '=', product.id)])
                    product_product.write(product_product_vals)

                    # _______ SIZE ________________
                    if attribute_size and value_size:
                        line = self.env['product.attribute.line'].search([
                            ('attribute_id', '=', attribute_size.id),
                            ('product_tmpl_id', '=', product.id)])
                        if len(line) == 0:
                            line = self.env['product.attribute.line'].create({
                                'attribute_id': attribute_size.id,
                                'product_tmpl_id': product.id,
                                'value_id0s': [(4, value_size.id)]})
                        else:
                            line.write({'value_ids': [(4, value_size.id)]})
                        product_size_vals.update({'attribute_value_ids': [(4, value_size.id)]})
                        product_product.write(product_size_vals)

                    # # ______________ COLOUR ________________
                    # if attribute_colour and value_colour:
                    #     line = self.env['product.attribute.line'].search([
                    #         ('attribute_id', '=', attribute_colour.id),
                    #         ('product_tmpl_id', '=', product.id)])
                    #     if len(line) == 0:
                    #         line = self.env['product.attribute.line'].create({
                    #             'attribute_id': attribute_colour.id,
                    #             'product_tmpl_id': product.id,
                    #             'value_ids': [(4, value_colour.id)]})
                    #     else:
                    #         line.write({'value_ids': [(4, value_colour.id)]})
                    #     product_colour_vals.update({'attribute_value_ids': [(4, value_colour.id)]})
                    #     product_product.write(product_colour_vals)

                else:
                    product.write(product_vals)
                    product_product = self.env['product.product'].search([
                        ('product_tmpl_id', '=', product.id), ('barcode', '=', product_product_vals['barcode'])])
                    if len(product_product) == 0:
                        product_product_vals.update({'product_tmpl_id': product.id})
                        product_product = self.env['product.product'].create(product_product_vals)
                    else:
                        product_product.write(product_product_vals)

                    # _______ SIZE ________
                    if attribute_size and value_size:
                        line = self.env['product.attribute.line'].search([
                            ('attribute_id', '=', attribute_size.id),
                            ('product_tmpl_id', '=', product.id)])
                        if len(line) == 0:
                            line = self.env['product.attribute.line'].create({
                                'attribute_id': attribute_size.id,
                                'product_tmpl_id': product.id,
                                'value_ids': [(4, value_size.id)]})
                        else:
                            line.write({'value_ids': [(4, value_size.id)]})

                        product_size_vals.update({
                            'product_tmpl_id': product.id,
                            'attribute_value_ids': [(4, value_size.id)],
                        })
                        product_product.write(product_size_vals)

                    # # ________ COLOUR ____________
                    # if attribute_colour and value_colour:
                    #     line = self.env['product.attribute.line'].search([
                    #         ('attribute_id', '=', attribute_colour.id),
                    #         ('product_tmpl_id', '=', product.id)])
                    #     if len(line) == 0:
                    #         line = self.env['product.attribute.line'].create({
                    #             'attribute_id': attribute_colour.id,
                    #             'product_tmpl_id': product.id,
                    #             'value_ids': [(4, value_colour.id)]})
                    #     else:
                    #         line.write({'value_ids': [(4, value_colour.id)]})
                    #
                    #     product_colour_vals.update({
                    #         'product_tmpl_id': product.id,
                    #         'attribute_value_ids': [(4, value_colour.id)],
                    #     })
                    #     product_product.write(product_colour_vals)


            cont += 1

        os.remove('/tmp/shoe_embassy_product1.csv')
        return True


