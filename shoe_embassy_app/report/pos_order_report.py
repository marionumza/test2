# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, tools


class PosOrderReport(models.Model):
    _inherit = "report.pos.order"

    user_id = fields.Many2one('res.users', string='Sales Person', readonly=True)
    sperson_id = fields.Many2one('res.partner', string='Salesman',help='The product used to model the discount.')
    model_id = fields.Many2one('shoe.embassy.model', string='Model', readonly=True)
    gender = fields.Selection([('Womens', 'Womens'), ('Mens', 'Mens')], string='Gender M/F')
    gender_id = fields.Many2one('gender', string='Order Group')
    shoe_type_id = fields.Many2one('shoe.embassy.shoe_type', string='Shoe Type', readonly=True)
    shoe_attribute_value_id = fields.Many2one('product.attribute.value', string='Size', required=True)
    #shoe_seller_id = fields.Many2one('res.partner', string='Supplier', readonly=True)
    brand_id = fields.Many2one('shoe.embassy.brand', string='Brand', readonly=True)
    price_sub_total_wo_tax = fields.Float(string='Subtotal w/o Tax', readonly=True,)
    collection_id = fields.Many2one('shoe.embassy.collection', string='Collection', readonly=True,)
    cost = fields.Float(readonly=1)
    cogs = fields.Float(readonly=1, string="COGS %", group_operator="avg")


    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self._cr, 'report_pos_order')
        # self._cr.execute("""
        #     CREATE OR REPLACE VIEW report_pos_order AS (
        #         SELECT
        #             MIN(l.id) AS id,
        #             COUNT(*) AS nbr_lines,
        #             s.date_order AS date,
        #             SUM(l.qty) AS product_qty,
        #             SUM(l.qty * l.price_unit) AS price_sub_total,
        #             SUM((l.qty * l.price_unit) * (100 - l.discount) / 100) AS price_total,
        #             SUM(l.qty *
        #                 (((l.qty * l.price_unit) * (100 - l.discount) / 100) / (
        #                     (100 + (
        #                         SELECT SUM(tax.amount) from account_tax tax
        #                         where tax.id IN(
        #                             Select rel.account_tax_id from account_tax_pos_order_line_rel rel
        #                             where rel.pos_order_line_id = l.id
        #                             )
        #                         )
        #                         ) / 100.0)
        #                     )
        #                 ) AS price_sub_total_wo_tax,
        #             SUM((l.qty * l.price_unit) * (l.discount / 100)) AS total_discount,
        #             (SUM(l.qty*l.price_unit)/SUM(l.qty * u.factor))::decimal AS average_price,
        #             SUM(cast(to_char(date_trunc('day',s.date_order) - date_trunc('day',s.create_date),'DD') AS INT)) AS delay_validation,
        #             s.id as order_id,
        #             s.partner_id AS partner_id,
        #             s.state AS state,
        #             s.user_id AS user_id,
        #             s.sperson_id AS sperson_id,
        #             s.location_id AS location_id,
        #             s.company_id AS company_id,
        #             s.sale_journal AS journal_id,
        #             l.product_id AS product_id,
        #             pt.categ_id AS product_categ_id,
        #             p.product_tmpl_id,
        #             ps.config_id,
        #             pt.pos_categ_id,
        #             pc.stock_location_id,
        #             s.pricelist_id,
        #             s.session_id,
        #             pt.shoe_type_id,
        #             pt.gender,
        #             pt.gender_id,
        #             pt.model_id,
        #             pt.brand_id,
        #             p.shoe_attribute_value_id,
        #             pt.shoe_seller_id,
        #             pt.collection_id,
        #             p.cost,
        #             ROUND(
        #                 CAST((
        #                     (
        #                     p.cost / NULLIF(
        #                         SUM(l.qty *
        #                                     (((l.qty * l.price_unit) * (100 - l.discount) / 100) / (
        #                                         (100 + (
        #                                             SELECT SUM(tax.amount) from account_tax tax
        #                                             where tax.id IN(
        #                                                 Select rel.account_tax_id from account_tax_pos_order_line_rel rel
        #                                                 where rel.pos_order_line_id = l.id
        #                                                 )
        #                                             )
        #                                             ) / 100.0)
        #                                         )
        #                                     )
        #                         , 0)
        #                     ) * 100) as numeric),1
        #                          ) as cogs,

        #             s.invoice_id IS NOT NULL AS invoiced
        #         FROM pos_order_line AS l
        #             LEFT JOIN pos_order s ON (s.id=l.order_id)
        #             LEFT JOIN product_product p ON (l.product_id=p.id)
        #             LEFT JOIN product_template pt ON (p.product_tmpl_id=pt.id)
        #             LEFT JOIN product_uom u ON (u.id=pt.uom_id)
        #             LEFT JOIN pos_session ps ON (s.session_id=ps.id)
        #             LEFT JOIN pos_config pc ON (ps.config_id=pc.id)
        #         GROUP BY
        #             s.id, s.date_order, s.partner_id,s.state, pt.categ_id,
        #             s.user_id, s.location_id, s.company_id, s.sale_journal,
        #             s.pricelist_id, s.invoice_id, s.create_date, s.session_id,
        #             l.product_id,
        #             pt.categ_id, pt.pos_categ_id,
        #             p.product_tmpl_id,
        #             ps.config_id,
        #             pc.stock_location_id,
        #             pt.shoe_type_id,
        #             pt.gender,
        #             pt.gender_id,
        #             p.shoe_attribute_value_id,
        #             pt.shoe_seller_id,
        #             pt.model_id,
        #             pt.brand_id,
        #             p.cost,
        #             pt.collection_id
        #         HAVING
        #             SUM(l.qty * u.factor) != 0
        #     )
        # """)
