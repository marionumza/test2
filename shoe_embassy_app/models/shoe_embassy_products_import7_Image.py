# -*- coding: utf-8 -*-


from odoo import models, fields, osv, api
from odoo.exceptions import ValidationError
from datetime import datetime, date
import os
import unicodedata
import requests
from odoo.tools.translate import _
import csv
import base64

class ShoeEmbassyProductImport7Image(models.Model):
    _name = "shoe.embassy.product.import7.image"

    name = fields.Char('Note')
    file = fields.Binary('Import Magento Image File')
    filename = fields.Char()

    def shoe_embassy_product_import_from_csv(self):

        fo = open('/tmp/shoe_embassy_product7.csv', 'wb+')
        fo.write(base64.b64decode(self.file))
        fo.close()
        f = open('/tmp/shoe_embassy_product7.csv')
        csv_f = csv.reader(f)
        cont = 1
        headers = []
        for row in csv_f:
            print cont
            if cont == 1:
                cont += 1
                for r in row:
                    headers.append(r)
                continue
            else:
                cell_count = 0
                product_vals = {}

                #item-id
                product = self.env['product.template'].search([('conf_id', '=', unicode(row[cell_count]))])
                cell_count += 1

                # product_image
                r = requests.get(unicode(row[cell_count]))
                image_base64 = base64.encodestring(r.content)
                product_vals.update({'image': image_base64})
                cell_count += 1

                if len(product) == 0:
                    print "New product found"
                else:
                    try:
                        product.write(product_vals)
                    except:
                        raise ValidationError(_('Error occurs on the line ' + str(cont)))

            cont += 1

        os.remove('/tmp/shoe_embassy_product7.csv')
        return True

