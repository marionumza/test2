# -*- coding: utf-8 -*-


from odoo import models, fields, osv, api
from odoo.exceptions import ValidationError
from datetime import datetime, date
import os
import unicodedata
import requests
from odoo.tools.translate import _
import csv
import base64


class ProductTemplate(models.Model):
    _inherit = "product.template"


    brand = fields.Char(string='Brand Name')
    conf_id = fields.Integer(string='Magento ID')
    shoe_type = fields.Char(string='Shoe Type')

