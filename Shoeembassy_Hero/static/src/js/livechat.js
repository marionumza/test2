odoo.define('Shoeembassy_Hero.im_livechat', function (require) {
"use strict";

var im_livechat = require('im_livechat.im_livechat');
var config = require('web.config');
var bus = require('bus.bus').bus;
var core = require('web.core');
var session = require('web.session');
var time = require('web.time');
var utils = require('web.utils');
var ChatWindow = require('mail.ChatWindow'); // including the new inherited Window
var Widget = require('web.Widget');
var mailchat = require('mail.ChatThread');

var _t = core._t;
var QWeb = core.qweb;

var _super_live_chat_button = im_livechat.LivechatButton.prototype;
    im_livechat.LivechatButton.include({
        close_chat: function () {
            this._super()
            this.channel.state="close";
        },
    });

    _super_live_chat_button.start =  function () {
        var self = this
        this.$el.text(this.options.button_text);
        var small_screen = config.device.size_class === config.device.SIZES.XS;
        if (self.history) {
            _.each(self.history.reverse(), self.add_message.bind(this));
            self.open_chat();
        } else if (!small_screen && this.rule.action === 'auto_popup') {
            var auto_popup_cookie = utils.get_cookie('im_livechat_auto_popup');
            if (!auto_popup_cookie || JSON.parse(auto_popup_cookie)) {
                self.auto_popup_timeout = setTimeout(self.open_chat.bind(this), self.rule.auto_popup_timer*1000);
            }
        }
        bus.on('notification', this, function (notifications) {
            var self = this;
            _.each(notifications, function (notification) {
                if (self.channel && (notification[0] === self.channel.uuid)) {
                    self.add_message(notification[1]);
                    self.render_messages();
                    if (self.chat_window.folded || !self.chat_window.thread.is_at_bottom()) {
                        self.chat_window.update_unread(self.chat_window.unread_msgs+1);
                    }
                }
            });
        });
        this.opening_chat = true;
        jQuery(document).ready(function(){
            $('div .chat_icon a').click(function(e){
                if (!self.channel){
                    var def = session.rpc('/im_livechat/get_session', {
                        channel_id : self.options.channel_id,
                        anonymous_name : self.options.default_username,
                    }, {shadow: true});
                    var cookie = utils.get_cookie('im_livechat_session');
                    self.willStart();
                    def.then(function (channel) {
                        if (!channel || !channel.operator_pid) {
                            alert(_t("None of our collaborators seems to be available, please try again later."));
                        } else {
                            self.channel = channel;
                            self.channel.state = "first";
                            self.open_chat_window(channel);
                            self.send_welcome_message();
                            self.render_messages();
                            bus.add_channel(channel.uuid);
                            bus.start_polling();

                            utils.set_cookie('im_livechat_session', JSON.stringify(channel), 60*60);
                            utils.set_cookie('im_livechat_auto_popup', JSON.stringify(false), 60*60);
                        }
                    }).always(function () {
                        self.opening_chat = false;
                    });
                }

               if (self.channel){
                    if(jQuery(".o_chat_window").css('height') == "400px"){
                        $(".o_chat_window").css('height',28);
                    }
                    else{
                        if(jQuery(".o_chat_window").css('height') == "28px"){
                            $(".o_chat_window").css('height',400);
                        }
                    }
                    if(jQuery(".o_chat_window").css('height') != "400px" && jQuery(".o_chat_window").css('height') != "28px"){
                        self.open_chat_window(self.channel)
                        self.render_messages();
                    }
                }
            });
        });
    };
});
