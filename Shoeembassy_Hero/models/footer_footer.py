# -*- coding: utf-8 -*-

from odoo import models, fields


class Footer(models.Model):

	_name = 'footer.footer'
	_rec_name = 'name'
	_order = 'sequence'

	sequence = fields.Integer(string='Sequence')
	name = fields.Char(string='Name')
	is_active = fields.Boolean('Active')
	url = fields.Char('URL')