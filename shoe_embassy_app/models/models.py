# -*- coding: utf-8 -*-


from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
from datetime import *
from dateutil.relativedelta import relativedelta


# class ShoeEmbassyHandle(models.Model):
#     _name = 'shoe.embassy.handle'
#
#     name = fields.Char(string='Handle')


class ShoeEmbassyBrand(models.Model):
    _name = 'shoe.embassy.brand'

    name = fields.Char(string='Brand')
    sequence = fields.Integer(string='Sequence')


class ShoeEmbassyModel(models.Model):
    _name = 'shoe.embassy.model'

    name = fields.Char(string='Model')


class ShoeEmbassyCollection(models.Model):
    _name = 'shoe.embassy.collection'

    name = fields.Char(string='Collection')


class ShoeEmbassyColour(models.Model):
    _name = 'shoe.embassy.colour'

    name = fields.Char(string='Colour')


class ShoeEmbassyColourCode(models.Model):
    _name = 'shoe.embassy.colour.code'

    colour_id = fields.Many2one('shoe.embassy.colour', string='Colour')
    name = fields.Char(string='Colour Code')


class ShoeEmbassyColourText(models.Model):
    _name = 'shoe.embassy.colour_text'

    name = fields.Char(string='Colour Text')


class ShoeEmbassyInfoColour(models.Model):
    _name = 'shoe.embassy.info_colour'

    name = fields.Char(string='Info Colour')


class ShoeEmbassyInfoDecoration(models.Model):
    _name = 'shoe.embassy.info_decoration'

    name = fields.Char(string='Info Decoration')


class ShoeEmbassyInfoDescription(models.Model):
    _name = 'shoe.embassy.info_description'

    name = fields.Char(string='Info Description')


class ShoeEmbassyInfoHeelHeight(models.Model):
    _name = 'shoe.embassy.info_heel_height'

    name = fields.Char(string='Info Heel Height')


class ShoeEmbassyInfoInside(models.Model):
    _name = 'shoe.embassy.info_inside'

    name = fields.Char(string='Info Inside')


class ShoeEmbassyInfoInsole(models.Model):
    _name = 'shoe.embassy.info_insole'

    name = fields.Char(string='Info Insole')


class ShoeEmbassyInfoMaterialHeel(models.Model):
    _name = 'shoe.embassy.info.material_heel'

    name = fields.Char(string='Info Material Heel')


class ShoeEmbassyInfoPlatformHeight(models.Model):
    _name = 'shoe.embassy.info_platform_height'

    name = fields.Char(string='Info Platform Height')


class ShoeEmbassyInfoSole(models.Model):
    _name = 'shoe.embassy.info_sole'

    name = fields.Char(string='Info Sole')


class ShoeEmbassyInfoSoleThickness(models.Model):
    _name = 'shoe.embassy.info_sole_thickness'

    name = fields.Char(string='Info Sole Thickness')


class ShoeEmbassyInfoTotalHeight(models.Model):
    _name = 'shoe.embassy.info_total_height'

    name = fields.Char(string='Info Total Height')


class ShoeEmbassyInfoUpper(models.Model):
    _name = 'shoe.embassy.info_upper'

    name = fields.Char(string='Info Upper')


class ShoeEmbassyInfoWeight(models.Model):
    _name = 'shoe.embassy.info_weight'

    name = fields.Char(string='Info Weight')


class ShoeEmbassyInfoWidth(models.Model):
    _name = 'shoe.embassy.info_width'

    name = fields.Char(string='Info Width')


class ShoeEmbassyShoeType(models.Model):
    _name = 'shoe.embassy.shoe_type'

    name = fields.Char(string='Shoe Type')


class ShoeEmbassyContactType(models.Model):
    _name = 'shoe.embassy.contact_type'

    name = fields.Char(string='Contact Type')


class ShoeEmbassyBarcode(models.Model):
    _name = 'shoe.embassy.barcode'

    name = fields.Char(string='Barcode', size=30)
    product_id = fields.Many2one('product.product', 'Used in product')


class ShoeEmbassyMatchBarcode(models.Model):
    _name = 'shoe.embassy.match.barcode'

    def match_barcode(self):
        barcodes = self.env['shoe.embassy.barcode'].search([('product_id', '=', False)])
        for barcode in barcodes:
            product = self.env['product.product'].search([('barcode', '=', barcode.name)])
            if len(product) != 0:
                barcode.write({'product_id': product.id})
        return True

    name = fields.Text(string='Message', size=30)


class ShoeEmbassyLifeCycle(models.Model):
    _name = 'shoe.embassy.life_cycle'

    name = fields.Char(string='Life Cycle')


class ShoeEmbassyPerformance(models.Model):
    _name = 'shoe.embassy.performance'

    name = fields.Char(string='Performance')


class ShoeEmbassyLining(models.Model):
    _name = 'shoe.embassy.lining'

    name = fields.Char(string='Lining')


class ShoeEmbassyManagementCategory(models.Model):
    _name = 'shoe.embassy.management_category'

    name = fields.Char('Name', index=True, required=True, translate=True)
    parent_id = fields.Many2one('shoe.embassy.management_category', 'Parent Category', index=True, ondelete='cascade')
    child_id = fields.One2many('shoe.embassy.management_category', 'parent_id', 'Child Categories')


class ShoeEmbassySpecialCategory(models.Model):
    _name = 'shoe.embassy.special_category'

    name = fields.Char('Name', index=True, required=True, translate=True)


class ShoeEmbassyOutlet(models.Model):
    _name = 'shoe.embassy.outlet'

    name = fields.Char(string='Outlet')


class ShoeEmbassyTag(models.Model):
    _name = 'shoe.embassy.tag'

    name = fields.Char(string='Tag')

class ShoeEmbassyTarget(models.Model):
    _name = 'shoe.embassy.target'

    product_id = fields.Many2one('product.product', 'Product')
    weekly_target = fields.Float('Weekly Target')
    daily_target = fields.Float('Daily Target')
    warehouse_id = fields.Many2one('stock.warehouse', string='Warehouse')