# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import logging
from datetime import timedelta
from functools import partial

import psycopg2

from odoo import api, fields, models, tools, _
from odoo.tools import float_is_zero
from odoo.exceptions import UserError
from odoo.http import request
import odoo.addons.decimal_precision as dp

_logger = logging.getLogger(__name__)


class PosOrder(models.Model):
    _inherit = "pos.order"

    def _prepare_analytic_account(self, line):
        warehouse = self.env['stock.warehouse'].search([('lot_stock_id', '=', line.order_id.location_id.id)])
        tag = self.env['account.analytic.tag'].search([('name', '=', warehouse.name)])
        account = self.env['account.analytic.account'].search([('tag_ids', 'in', tag.id)])
        if len(account) > 1 and len(tag) == 1:
            raise ValueError(_("There are more then one Analytic Account with tag " + tag.name))


        return account.id
        #return False

    def _action_create_invoice_line(self, line=False, invoice_id=False):
        result = super(PosOrder,self)._action_create_invoice_line(line, invoice_id)
        return result