# -*- coding: utf-8 -*-

from odoo import models, fields


class OurOffefs(models.Model):
	_name = 'our.offers'
	_rec_name = 'offers_desc'

	offers_desc = fields.Char('Offers Description')
	is_active = fields.Boolean('Active')
	rotation_speed = fields.Integer('Rotation Speed')