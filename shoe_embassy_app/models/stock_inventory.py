# -*- coding: utf-8 -*-


from odoo import models, fields, osv, api
from odoo.exceptions import ValidationError
from datetime import datetime, date
import os


from odoo.tools.translate import _
from odoo.osv import osv
import json
import ast

import csv
import base64

class StockInventory(models.Model):
    _inherit = 'stock.inventory'

    @api.multi
    def action_start(self):
        res = super(StockInventory, self).action_start()
        self.action_reset_product_qty()
        return res

    prepare_inventory = action_start


    @api.model
    def open_new_inventory(self):
        action = self.env.ref('stock_barcode.stock_inventory_action_new_inventory').read()[0]
        # if self.env.ref('stock.warehouse0', raise_if_not_found=False):
        #     new_inv = self.env['stock.inventory'].create({
        #         'filter': 'partial',
        #         'name': fields.Date.context_today(self),
        #     })
        #     new_inv.prepare_inventory()
        #     action['res_id'] = new_inv.id
        action['context'] = {'default_filter': 'none', 'default_location_id': False}
        return action


# class StockInventoryWizard(models.TransientModel):
#     _name = 'stock.inventory.wizard'
#
#
#     def procede(self):
#         return self.env['stock.inventory'].open_new_inventory_cont(self.name)
#
#     name = fields.Char(u'Scanning...')