# -*- coding: utf-8 -*-

{
    "name": "Extra product fields",
    "summary": "",
    "version": "1.0.0",
    "category": "Inventory",
    "author": "Anywhereconnex",
    "license": "AGPL-3",
    "application": True,
    "installable": True,
    "images": [],
    "depends": ["product"],
    'data': [
        'views/product_view.xml'
    ],
    'currency': 'EUR'
}
