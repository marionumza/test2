# -*- coding: utf-8 -*-
# Part of Softhealer Technologies.

from odoo import models,fields,api
class website_faq(models.Model):
    _name="website.faq"
    
    name = fields.Char(string="FAQ Title",required=True)
    active = fields.Boolean(string="Active")
    faq_lines = fields.One2many("website.faq.line","website_faq_id",string="FAQ line")
    
class website_faq_line(models.Model):
    _name="website.faq.line"
    
    que = fields.Char(string="Question",required=True)
    ans = fields.Html(string="Answer",required=True)
    is_publish = fields.Boolean(string="Publish",default=True)
    website_faq_id = fields.Many2one("website.faq", string="FAQ ID")