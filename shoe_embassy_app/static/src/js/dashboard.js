odoo.define('shoe_embassy_app.dashboard', function (require) {
"use strict";

var ActionManager = require('web.ActionManager');
var core = require('web.core');
var data = require('web.data');
var Dialog = require('web.Dialog');
var FavoriteMenu = require('web.FavoriteMenu');
var form_common = require('web.form_common');
var Model = require('web.DataModel');
var pyeval = require('web.pyeval');
var ViewManager = require('web.ViewManager');

var _t = core._t;
var QWeb = core.qweb;

var CustomDashBoard = DashBoard.extend({
    events: {
//        'click .oe_dashboard_link_change_layout': 'on_change_layout',
        'click .oe_dashboard_link_refresh': 'refresh',
//        'click h2.oe_header span.oe_header_txt': function (ev) {
//            if(ev.target === ev.currentTarget)
//                this.on_header_string($(ev.target).parent());
//        },
//        'click .oe_dashboard_column .oe_fold': 'on_fold_action',
//        'click .oe_dashboard_column .oe_close': 'on_close_action',
    },
    refresh: function() {
        window.location.reload(true);
    },

});
})