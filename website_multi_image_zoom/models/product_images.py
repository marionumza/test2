# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.

from odoo import fields, models,api,_
import urllib2
import base64

class ProductImage(models.Model):
    _name = 'product.image'
    _description = 'Product Image'
    _order = 'type'
    
    name = fields.Char(string='Name')
    description = fields.Text(string='Description')
    image_alt = fields.Text(string='Image Label')
    image = fields.Binary(string='Image')
    is_active = fields.Boolean('Active', default=True)
#     image = fields.Binary(compute='_image_store_function_field',string='image',store=False)
    image_small = fields.Binary(string='Small Image')
    image_url = fields.Char(string='Image URL')
    type = fields.Selection( [('c','Category'),
                              ('p1','Side Front'),
                              ('p2','Side Left'),
                              ('p3','Top'),
                              ('p4','Bottom'),
                              ('p5','Side Back'),
                              ('p6','2 Shoes'),
                              ('p7','Product 7'),
                              ('p8','Product 8'),
                              ('p9','Product 9'),
                              ('p10','Product 10'),
                              ('p11','Product 11'),
                              ('p12','Product 12'),
                              ('p13','Product 13'),
                              ('p14','Product 14'),
                              ('p15','Product 15')
                              ],'Type')
    product_tmpl_id = fields.Many2one('product.template', 'Product',
                                      copy=False)
    product_variant_id = fields.Many2one('product.product', 'Product Variant',
                                         copy=False)
#     @api.multi
#     @api.depends('image_url')
#     def _image_store_function_field(self):
#         for i in self:
#             if i.image_url:
#                 i.image = base64.encodestring(urllib2.urlopen(i.image_url).read())
#         return True


class ProductProduct(models.Model):
    _inherit = 'product.product'

    images_variant = fields.One2many('product.image', 'product_variant_id',
                                     'Images')


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    images = fields.One2many('product.image', 'product_tmpl_id', 'Images')
    variant_bool = fields.Boolean(string='Show Variant Wise Images',
                                  help='Check if you like to show variant wise'
                                       ' images in WebSite', auto_join=True)
