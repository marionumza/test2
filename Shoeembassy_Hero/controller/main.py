# -*- coding: utf-8 -*-

from odoo import http, tools, _
from odoo.http import request

from odoo.addons.website_sale.controllers.main import WebsiteSale
from odoo.addons.website_sale.controllers import main
from odoo.addons.website.controllers.main import QueryURL
from odoo.addons.website.models.website import slug
from collections import defaultdict


PPG = 21
PPR = 3


class WebsiteSaleInherit(WebsiteSale):

    def _get_search_domain(self, search, category, attrib_values):
        domain = request.website.sale_product_domain()
        if search:
            for srch in search.split(" "):
                domain += [
                    '|', '|', '|', '|',('name', 'ilike', srch), ('description', 'ilike', srch),
                    ('description_sale', 'ilike', srch), ('product_variant_ids.default_code', 'ilike', srch), ('brand_id.name', 'ilike', srch)]

        if category:
            domain += [('public_categ_ids', 'child_of', int(category))]

        if attrib_values:
            attrib = None
            ids = []
            for value in attrib_values:
                if not attrib:
                    attrib = value[0]
                    ids.append(value[1])
                elif value[0] == attrib:
                    ids.append(value[1])
                else:
                    domain += [('attribute_line_ids.value_ids', 'in', ids)]
                    attrib = value[0]
                    ids = [value[1]]
            if attrib:
                domain += [('attribute_line_ids.value_ids', 'in', ids)]
#         domain += [('qty_available', '>', 0)]
        return domain

    # def _get_search_order(self, post):
    #     if post.get('order') and 'brand_id asc' in post.get('order'):
    #         print '----post-----', post.get('order', 'website_sequence desc')
    #         return 'brand_id asc, website_published asc , id desc'
    #     else:
    #         return '%s, website_published desc , id desc' % post.get('order', 'website_sequence desc')

    @http.route([
        '/shop',
        '/shop/page/<int:page>',
        '/shop/category/<model("product.public.category"):category>',
        '/shop/category/<model("product.public.category"):category>/page/<int:page>'
    ], type='http', auth="public", website=True)
    def shop(self, page=0, category=None, search='', ppg=False, **post):
        if ppg:
            try:
                ppg = int(ppg)
            except ValueError:
                ppg = PPG
            post["ppg"] = ppg
        else:
            ppg = PPG
        product_list = []    
        avail_list = []
        attrib_list = request.httprequest.args.getlist('attrib')
        attrib_values = [map(int, v.split("-")) for v in attrib_list if v]
        attributes_ids = set([v[0] for v in attrib_values])
        attrib_set = set([v[1] for v in attrib_values])

        domain = self._get_search_domain(search, category, attrib_values)
        if category:
            category = request.env['product.public.category'].search([('id', '=', int(category))], limit=1)
            categ_obj = request.env['product.public.category'].sudo().search([('parent_id','=',int(category))])
            if not categ_obj:
                categ_obj = [category]
            for categ in categ_obj:
                product_ids = request.env['product.template'].sudo().search(domain+[('public_categ_ids','=',categ.id)], order=self._get_search_order(post))
                if product_ids:
                    product_list = product_list+[categ]+[product for product in product_ids]
                    avail_list.append(categ.id)
            if not category:
                raise NotFound()
             
        keep = QueryURL('/shop', category=category and int(category), search=search, attrib=attrib_list, order=post.get('order'))
        pricelist_context = dict(request.env.context)
        if not pricelist_context.get('pricelist'):
            pricelist = request.website.get_current_pricelist()
            pricelist_context['pricelist'] = pricelist.id
        else:
            pricelist = request.env['product.pricelist'].browse(pricelist_context['pricelist'])

        request.context = dict(request.context, pricelist=pricelist.id, partner=request.env.user.partner_id)

        url = "/shop"
        if search:
            post["search"] = search
        if attrib_list:
            post['attrib'] = attrib_list
        categs = request.env['product.public.category'].search([('parent_id', '=', False)])
        Product = request.env['product.template']

        parent_category_ids = []
        if category:
            url = "/shop/category/%s" % slug(category)
            parent_category_ids = [category.id]
            current_category = category
            while current_category.parent_id:
                parent_category_ids.append(current_category.parent_id.id)
                current_category = current_category.parent_id

#         product_count = Product.search_count(domain)
        pager = request.website.pager(url=url, total=len(product_list), page=page, step=ppg, scope=10000, url_args=post)
#         products_old = Product.search(domain, limit=ppg, offset=pager['offset'], order=self._get_search_order(post))
        products_old = product_list[pager['offset']:pager['offset']+20]
        ProductAttribute = request.env['product.attribute']
        ProductAttributeValue = request.env['product.attribute.value']
        inventory_products = products_old
        # get all products without limit
#             quant_dict = {}
#             selected_products = Product.search(domain, limit=False)
#             quant_obj = request.env['stock.quant'].sudo().search([])
#             quant_ids = quant_obj.filtered(lambda x: x.product_id.product_tmpl_id.id in selected_products._ids)
#             for quant in quant_ids:
#                 quant_dict.update({quant: quant.product_id})
#             v = defaultdict(list)
#             for key, value in sorted(quant_dict.iteritems()):
#                 v[value].append(key)
#             final_dict = dict(v)
#             for prod,quant_ids in final_dict.iteritems():
#                 prod_qty = 0
#                 for rec in quant_ids:
#                     prod_qty += rec.qty
#                 
#                 if prod_qty > 0:
#                     inventory_products.append(prod.product_tmpl_id)
        
        # attributes = ProductAttribute.search([])
        # e = [x.id for x in inventory_products]
        # attributes = ProductAttribute.search([('attribute_line_ids.product_tmpl_id', 'in', e)])
        # attr_vals_ids = ProductAttributeValue.sudo().search([])
        # all_pr_get = request.env['product.template'].search([('id', 'in', e), ('public_categ_ids', 'in', avail_list)])
        # attributes_vals = ProductAttributeValue.sudo().search([('product_ids.product_tmpl_id', 'in', all_pr_get.ids)])
        # not_attr = attr_vals_ids.filtered(lambda x:x.id not in attributes_vals.ids)
        # for attr_vals in attributes_vals:
        #     attr_vals.is_used = True
        # for x in not_attr:
        #     x.is_used = False

        attributes = ProductAttribute.browse(attributes_ids)
#         products = products_old.filtered(lambda x: x in inventory_products)
        from_currency = request.env.user.company_id.currency_id
        to_currency = pricelist.currency_id
        compute_currency = lambda price: from_currency.compute(price, to_currency)
        product_data_list = []
        for index,value in enumerate(products_old):
            if index == 0:
                product_data_list.append([value])
            elif value._name == product_data_list[-1][-1]._name:
                product_data_list[-1].append(value)
            else:
                product_data_list.append([value])
        attr_list = []
        if category:
            if not category.parent_id:
                for attr in category.attribute_value_ids:
                    if attr.attribute_id not in attr_list:
                        attr_list.append(attr.attribute_id)
            if category.parent_id:
                for attr in category.parent_id.attribute_value_ids:
                    if attr.attribute_id not in attr_list:
                        attr_list.append(attr.attribute_id)
        values = {
            'search': search,
            'avail_list':avail_list,
            'category': category,
            'attrib_values': attrib_values,
            'attrib_set': attrib_set,
            'pager': pager,
            'pricelist': pricelist,
            'attr_list': attr_list,
            'products': product_data_list,
            'search_count': len(product_data_list),  # common for all searchbox
#             'bins': main.TableCompute().process(product_list, ppg),
            'rows': PPR,
            'categories': categs,
            'attributes': attributes,
            'compute_currency': compute_currency,
            'keep': keep,
            'parent_category_ids': parent_category_ids,
        }
        if category:
            values['main_object'] = category
        return request.render("website_sale.products", values)
    