odoo.define('Shoeembassy_Hero.socialmedia_data', function (require) {
'use strict';
var ajax = require('web.ajax');
var core = require('web.core');
var base = require('web_editor.base');
var animation = require('web_editor.snippets.animation');
var no_of_product;
var qweb = core.qweb;
animation.registry.js_get_socialmedia=animation.Class.extend({selector:".js_get_socialmedia",start:function(){this.redrow()},stop:function(){this.clean()},redrow:function(t){this.clean(t),this.build(t)},clean:function(t){this.$target.empty()},build:function(t){var a=this,e=a.$target.data("template");e||(e="Shoeembassy_Hero.snippet_social_media");ajax.jsonRpc("/socialmedia_data","call",{template:e}).then(function(t){$(t).appendTo(a.$target)}).then(function(){}).fail(function(t){})}});

});
