# -*- coding: utf-8 -*-
#################################################################################
# Author      : Webkul Software Pvt. Ltd. (<https://webkul.com/>)
# Copyright(c): 2015-Present Webkul Software Pvt. Ltd.
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://store.webkul.com/license.html/>
#################################################################################
{
  "name"                 :  "Website Stock Notify",
  "summary"              :  "Send e-mail notifications to your customers, when product is in-stock.",
  "category"             :  "Website",
  "version"              :  "1.1",
  "sequence"             :  1,
  "author"               :  "Webkul Software Pvt. Ltd.",
  "license"              :  "Other proprietary",
  "website"              :  "https://store.webkul.com/Odoo-Website-Stock-Notify.html",
  "description"          :  """http://webkul.com/blog/website-stock-notify/""",
  "live_test_url"        :  "http://odoodemo.webkul.com/?module=website_stock_notifiy&version=10.0",
  "depends"              :  [
                             'website_stock',
                             'mail',
                             'wk_wizard_messages',
                            ],
  "data"                 :  [
                             'data/stock_notify_cron.xml',
                             'edi/website_notify_edi.xml',
                             'data/stock_action_server.xml',
                             'views/templates.xml',
                             'views/stock_notify_config_view.xml',
                             'views/website_stock_notification_view.xml',
                             'views/webkul_addons_config_inherit_view.xml',
                             'data/notify_config_demo.xml',
                            ],
  "images"               :  ['static/description/Banner.png'],
  "application"          :  True,
  "installable"          :  True,
  "auto_install"         :  False,
  "price"                :  15,
  "currency"             :  "EUR",
  "pre_init_hook"        :  "pre_init_check",
}