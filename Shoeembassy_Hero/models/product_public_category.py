# -*- coding: utf-8 -*-

from odoo import models, fields, tools, api, _
from odoo.exceptions import  ValidationError

class ProductPublicCategory(models.Model):

    _inherit = 'product.public.category'
    _order = 'sequence'

    is_active = fields.Boolean('Active')
    is_asc_subcateg = fields.Boolean('Asc Subcategory')
    show_category_image = fields.Boolean('Show Category Image')
    background_color = fields.Char('Background Color', help="Here you can set a "
    "specific HTML color index (e.g. #ff0000) to set the background color of sub category.")
    is_search_categ = fields.Boolean('Search Category')
    attribute_value_ids = fields.Many2many(
        'product.attribute.value', string='Attributes', ondelete='restrict')

    @api.multi
    @api.constrains('is_search_categ')
    def _check_search_categ(self):
        if len(self.search([]).filtered(lambda c: c.is_search_categ))>1:
            raise ValidationError(_('There can be only 1 search category'))