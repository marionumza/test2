# -*- coding: utf-8 -*-


from odoo import models, fields, osv, api
from odoo.exceptions import ValidationError
from datetime import datetime, date
import os


from odoo.tools.translate import _
from odoo.osv import osv
import json
import ast

import csv
import base64

class Shoe_Embassy_Black_Friday_Price_import(models.Model):
    _name = "shoe.embassy.black.friday.price.import"

    name = fields.Char('Note')
    file = fields.Binary('Import Price File')
    filename = fields.Char()

    

    def shoe_embassy_black_friday_price_import_from_csv(self):

        fo = open('/tmp/shoe_embassy_black_friday_price.csv', 'wb+')
        fo.write(base64.b64decode(self.file))
        fo.close()
        f = open('/tmp/shoe_embassy_black_friday_price.csv')
        csv_f = csv.reader(f)
        cont = 1
        headers = []
        for row in csv_f:
            # print cont
            if cont == 1:
                cont += 1
                for r in row:
                    headers.append(r)
                continue
            else:
                cell_count = 0
                message = ''

                # item - id
                product = self.env['product.template'].search([('conf_id', '=', row[cell_count])])
                if len(product) > 1:
                    raise ValidationError(_("There are more then one product with magento ID " + row[cell_count]))
                elif len(product) == 1:
                    product_product = self.env['product.product'].search([('product_tmpl_id', '=', product.id)])
                    cell_count += 8

                    pricelist = self.env['product.pricelist'].search([('name', '=', 'Black Friday Price')])
                    if len(pricelist) == 0:
                        pricelist = self.env['product.pricelist'].create({
                            'name': 'Black Friday Price',
                            'selectable': True,
                            'discount_policy': 'with_discount',
                        })
                    item = self.env['product.pricelist.item'].search([
                        ('product_tmpl_id', '=', product.id),
                        ('pricelist_id', '=', pricelist.id)
                    ])
                    if len(item) == 0:
                        product.write({'item_ids': [(0, 0, {
                            'applied_on': '1_product',
                            'product_tmpl_id': product.id,
                            'min_quantity': 1,
                            'compute_price': 'fixed',
                            'fixed_price': row[cell_count],
                            'pricelist_id': pricelist.id,
                            'base': 'list_price',
                            'date_end': '11/25/2017',
                            'date_start': '11/16/2017',
                        })]})
                    else:
                        item.write({
                            'applied_on': '1_product',
                            'min_quantity': 1,
                            'compute_price': 'fixed',
                            'fixed_price': row[cell_count],
                            'base': 'list_price',
                            'date_end': '11/25/2017',
                            'date_start': '11/16/2017',
                        })

                    product_product.write({'black_friday_price': row[cell_count]})

                else:
                    print row[cell_count] + ' ' + row[cell_count + 1] + ' Not Found'


            cont += 1

        os.remove('/tmp/shoe_embassy_black_friday_price.csv')
        self.write({'name': message})
        return True


