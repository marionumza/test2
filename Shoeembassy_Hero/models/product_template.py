# -*- coding: utf-8 -*-

from odoo import api, fields, models


class  ProductTemplate(models.Model):
    
    _inherit = 'product.template'
    
    # product_brand_name = fields.Char('Brand Name')
    # product_color = fields.Char('Color')
    # recommend_product_ids = fields.Many2many('product.template', string='Recommend Product')
    recommend_product_ids = fields.Many2many('product.template', 'product_recommend_rel', 'recommend_src_id', 'recommend_dest_id',
                                               string='Recommend Products')
  
    @api.multi
    def check_product_quantity(self, variant_id, value_id):
    	quantity = 0
    	if variant_id and value_id:
    		product_id = self.env['product.product'].sudo().search([('product_tmpl_id','=',self.id),('attribute_value_ids','in',value_id.ids)])
    		quantity = product_id and product_id[0] and product_id[0].sudo().qty_available
    	return quantity

    @api.multi
    def get_product_images(self):
    	product_type = ['c','p1','p2','p3','p4','p5','p6','p7','p8','p9','p10','p11','p12','p13','p14','p15']
    	image_list = []
    	for p_type in product_type:
    		for image in self.sudo().images:
    			if p_type == str(image.type):
    				if len(image_list) < 6:
    					image_list.append(image.id)
    	return self.env['product.image'].browse(image_list)
