## Website Product Stock

* __``` version- 9.0.1.0 ``` [Date:- 16-Nov-2016, Added by:- Webkul Software Pvt. Ltd.]__
	* Show product stock to both registered users and visitors, ie. user need not to login to see the product stock.
	* Display custom message for Product In Stock and Product Out of Stock.
	* Option to allow/disallow placing order for Out of Stock Product.
	* Set a custom message for any particular product for stock status.
	* Select the stock type to show on website.
	* Full multi language support

* __``` version- 9.0.1.1 ``` [Date:- 03-Feb-2017, Updated by:- Webkul Software Pvt. Ltd.]__
	* Fix internal server error for public and sign-up users
	* fix config from open issue
	* Improve coading and fix other small bug

* __``` version- 9.0.1.5 ``` [Date:- 17-Aug-2017, Updated by:- Webkul Software Pvt. Ltd.]__
	* Improve coading and fix other small bug
	* Fix compatibility issue