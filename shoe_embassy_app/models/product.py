# -*- coding: utf-8 -*-


from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
from datetime import *
from dateutil.relativedelta import relativedelta

class ProductTemplate(models.Model):
    _inherit = 'product.template'

    def compute_product_variant_count(self):
        for product in self.env['product.template'].search([]):
            product.create_variant_ids()
            product.product_variant_count = len(product.product_variant_ids)
        return True

    def compute_gender(self):
        products = self.env['product.template'].search([])
        for product in products:
            gender_id = self.env['gender'].search([('name', '=', product.gender)])
            print(product.name + ' - ' + (product.gender or 'Empty') + ' - ' + (gender_id.name or 'False'))
            product.gender_id = gender_id.id
        return True

    app = fields.Selection([('male', 'Male'), ('female', 'Female'), ('unisex', 'Unisex')], string="App")

    #handle_id = fields.Many2one('shoe.embassy.handle', string='Handle')
    brand_id = fields.Many2one('shoe.embassy.brand', string='Brand Name')
    model_id = fields.Many2one('shoe.embassy.model', string='Model')
    collection_id = fields.Many2one('shoe.embassy.collection', string='Collection')
    season = fields.Selection([
        ('Summer', 'Summer'), ('Spr-Aut', 'Spr-Aut'), ('Winter', 'Winter'), ('Mix', 'Mix')], string='Season')
    gender = fields.Selection([('Womens', 'Womens'), ('Mens', 'Mens')], string='Gender M/F')
    gender_id = fields.Many2one('gender', string='Order Group')
    end_life = fields.Boolean(string='End of Life')
    colour_id = fields.Many2one('shoe.embassy.colour', string='Colour')
    colour_code_id = fields.Many2one('shoe.embassy.colour.code', string='Colour Code')

    colour_text_id = fields.Many2one('shoe.embassy.colour_text', string='Colour Text')
    info_colour_id = fields.Many2one('shoe.embassy.info_colour', string='Info colour')
    info_decoration_id = fields.Many2one('shoe.embassy.info_decoration', string='Info Decoration')
    info_description_id = fields.Many2one('shoe.embassy.info_description', string='Info Description')
    info_heel_height_id = fields.Many2one('shoe.embassy.info_heel_height', string='Info Heel Height')
    info_inside_id = fields.Many2one('shoe.embassy.info_inside', string='Info Inside')
    info_insole_id = fields.Many2one('shoe.embassy.info_insole', string='Info Insole')
    info_material_heel_id = fields.Many2one('shoe.embassy.info.material_heel', string='Info Material Heel')
    info_platform_height_id = fields.Many2one('shoe.embassy.info_platform_height', string='Info Platform Height')
    info_sole_id = fields.Many2one('shoe.embassy.info_sole', string='Info Sole')
    info_sole_thickness_id = fields.Many2one('shoe.embassy.info_sole_thickness', string='Info Sole Thickness')
    info_total_height_id = fields.Many2one('shoe.embassy.info_total_height', string='Info Total Height')
    info_upper_id = fields.Many2one('shoe.embassy.info_upper', string='Info Upper')
    info_upper_circuit = fields.Char(string='Info Upper Circuit')
    info_width_id = fields.Many2one('shoe.embassy.info_width', string='Info Width')
    info_weight_id = fields.Many2one('shoe.embassy.info_weight', string='Info Weight')
    info_circuit_ankle = fields.Char(string='Info Height')
    product_sheep_catalog = fields.Selection([('Yes', 'Yes'), ('No', 'No')], string='Sheep Catalog')
    product_wool_catalog = fields.Selection([('Yes', 'Yes'), ('No', 'No')], string='Wool Catalog')
    shoe_type_id = fields.Many2one('shoe.embassy.shoe_type', string='Shoe Type')


    life_cycle_id = fields.Many2one('shoe.embassy.life_cycle', string='Life Cycle')
    performance_id = fields.Many2one('shoe.embassy.performance', string='Performance')
    lining_ids = fields.Many2many('shoe.embassy.lining', string='Lining')
    sub_categ_id = fields.Many2one('product.category', string='Internal Sub Category')
    management_categ_id = fields.Many2one('shoe.embassy.management_category', string='Sale Cycle')
    management_subcateg_id = fields.Many2one('shoe.embassy.management_category', string='Product Category')
    special_categ_id = fields.Many2one('shoe.embassy.special_category', string='Special Category')
    outlet_id = fields.Many2one('shoe.embassy.outlet', string='Outlet')
    new_site = fields.Selection([('yes', 'Yes'), ('no', 'No')], string='New Site')
    tag_ids = fields.Many2many('shoe.embassy.tag', string='Product Tags')

    min_stock_warning = fields.Integer(string='Minimum Stock Warning')
    min_stock_alert = fields.Boolean(string='Min stock alert')
    color = fields.Char()

    conf_id = fields.Integer(string='Magento ID')
    shoe_name = fields.Char(compute='_compute_shoe_name')

    # def _compute_quantities(self):
    #     res = self._compute_quantities_dict()
    #     for template in self:
    #         template.qty_available = res[template.id]['qty_available']
    #         template.virtual_available = res[template.id]['virtual_available']
    #         template.incoming_qty = res[template.id]['incoming_qty']
    #         template.outgoing_qty = res[template.id]['outgoing_qty']
    #         template.qty_available_store = res[template.id]['qty_available']
    #
    # qty_available_store = fields.Float(
    #     'Quantity On Hand(S)', compute='_compute_quantities', store=True)

    shoe_seller_id = fields.Many2one('res.partner', compute='get_seller', string="Seller", store=True)

    packaging_ids = fields.Many2many('product.packaging', string='Logistical Units')

    def update_seller(self):
        for product in self.env['product.template'].search([]):
            if len(product.seller_ids):
                product.shoe_seller_id = product.seller_ids[0].name.id
        return True

    @api.depends('seller_ids')
    def get_seller(self):
        for product in self:
            if len(product.seller_ids):
                product.shoe_seller_id = product.seller_ids[0].name.id

    @api.depends('name', 'brand_id.name', 'colour_id.name')
    def _compute_shoe_name(self):
        for product in self:
            if product.brand_id:
                product.shoe_name = (product.brand_id.name.encode('utf-8') or '') + '- ' + str(product.colour_id.name)
            else:
                product.shoe_name = str(product.colour_id.name)

    def sincronize_min_stock(self):
        # for reg in self.search([]):
        #     if reg.qty_available < reg.min_stock_warning:
        #         reg.min_stock_alert = True
        #         reg.color = '#FF0000'
        #     elif reg.qty_available == reg.min_stock_warning:
        #         reg.min_stock_alert = True
        #         reg.color = '#FFFF00'
        #     else:
        #         reg.min_stock_alert = False
        #         reg.color = '#FFFFFF'
        return True


class ProductProduct(models.Model):
    _inherit = 'product.product'

    barcode = fields.Char(
        'Barcode', copy=False, oldname='ean13', size=30,
        help="International Article Number used for product identification.")
    barcode_id = fields.Many2one('shoe.embassy.barcode', string='Collection')
    target_ids = fields.One2many('shoe.embassy.target', 'product_id', string='Targets')
    cost = fields.Float(string='Variant cost')
    special_price = fields.Float(string='Special Price')
    brand_id = fields.Many2one(related='product_tmpl_id.brand_id', string='Brand Name')
    simple_id = fields.Integer(string='Magento Simple ID')
    black_friday_price = fields.Float(string='Black Friday Price')
    shoe_attribute_value_id = fields.Many2one('product.attribute.value', compute='get_attribute', string="Size", store=True)

    # @api.depends('stock_quant_ids', 'stock_move_ids')
    # def _compute_quantities(self):
    #     res = self._compute_quantities_dict(self._context.get('lot_id'), self._context.get('owner_id'),
    #                                         self._context.get('package_id'), self._context.get('from_date'),
    #                                         self._context.get('to_date'))
    #     for product in self:
    #         product.qty_available = res[product.id]['qty_available']
    #         product.qty_available_store = res[product.id]['qty_available']
    #         product.incoming_qty = res[product.id]['incoming_qty']
    #         product.outgoing_qty = res[product.id]['outgoing_qty']
    #         product.virtual_available = res[product.id]['virtual_available']
    # qty_available_store = fields.Integer(compute='_compute_quantities', string='Quantity On Hand(S)', store=True)

    def update_size(self):
        for product in self.env['product.product'].search([]):
            if len(product.attribute_value_ids):
                product.shoe_attribute_value_id = product.attribute_value_ids[0].id
        return True

    @api.depends('attribute_value_ids')
    def get_attribute(self):
        for product in self:
            if len(product.attribute_value_ids):
                product.shoe_attribute_value_id = product.attribute_value_ids[0]

    @api.onchange('barcode')
    def onchange_barcode(self):
        if self.barcode == False:
            self.barcode_id = False
        else:
            barcode = self.env['shoe.embassy.barcode'].search([('name', '=', self.barcode)])
            if barcode:
                self.barcode_id = barcode.id
            else:
                self.barcode_id = False

    def write(self, vals):
        if 'barcode_id' in vals:
            if vals['barcode_id']:
                barcode = self.env['shoe.embassy.barcode'].browse(vals['barcode_id'])
                barcode.write({'product_id': self.id})
                vals['barcode'] = barcode.name
            else:
                barcode = self.env['shoe.embassy.barcode'].search([('product_id', '=', self.id)])
                barcode.write({'product_id': False})
        if 'barcode' in vals:
            barcode = self.env['shoe.embassy.barcode'].search([('name', '=', vals['barcode'])])
            if barcode:
                vals.update({'barcode_id': barcode.id})
                barcode.write({'product_id': self.id})
            else:
                vals.update({'barcode_id': False})
                barcode = self.env['shoe.embassy.barcode'].search([('product_id', '=', self.id)])
                barcode.write({'product_id': False})

        return super(ProductProduct, self).write(vals)

    def create(self, vals):
        if 'barcode' in vals:
            barcode = self.env['shoe.embassy.barcode'].search([('name', '=', self.barcode)])
            if barcode:
                vals.update({'barcode_id': barcode.id})
                barcode.write({'product_id': self.id})
            else:
                vals.update({'barcode_id': False})
                barcode = self.env['shoe.embassy.barcode'].search([('product_id', '=', self.id)])
                barcode.write({'product_id': False})
        return super(ProductProduct, self).create(vals)
