# -*- coding: utf-8 -*-


from odoo import models, fields, osv, api
from odoo.exceptions import ValidationError
from datetime import datetime, date
import os


from odoo.tools.translate import _
from odoo.osv import osv
import json
import ast

import csv
import base64

class ShoeEmbassyOrdersImport1Online(models.Model):
    _name = "shoe.embassy.orders.import1.online"

    name = fields.Char('Note')
    file = fields.Binary('Import Online Orders File')
    filename = fields.Char()

    

    def shoe_embassy_orders_import_from_csv(self):

        fo = open('/tmp/shoe_embassy_online_orders.csv', 'wb+')
        fo.write(base64.b64decode(self.file))
        fo.close()
        f = open('/tmp/shoe_embassy_online_orders.csv')
        csv_f = csv.reader(f)
        cont = 1
        headers = []
        for row in csv_f:
            print cont
            if cont == 1:
                cont += 1
                for r in row:
                    headers.append(r)
                continue
            else:
                cell_count = 0
                order_vals = {}
                order_line_vals = {}
                partner_vals = {}
                shipping_vals = {}
                billing_vals = {}
                payment_vals = {}

                # order - id
                #order_vals.update({'name': row[cell_count].encode('utf-8')})
                order_vals.update({'name': row[cell_count].decode('utf-8')})
                cell_count += 1

                # product - id
                magento = unicode(row[cell_count])
                product = self.env['product.product'].search([('simple_id', '=', unicode(row[cell_count]))])
                order_line_vals.update({'product_id': product.id or False})
                cell_count += 1

                # purchase - date
                order_vals.update({'date_order': unicode(row[cell_count])})
                cell_count += 1

                # payments - date
                payment_vals.update({'payment_date': unicode(row[cell_count])})
                # payment_vals.update({'payment_method_id': unicode(row[cell_count])})
                cell_count += 1

                # buyer - email
                partner_vals.update({'email': row[cell_count].decode('utf-8')})
                cell_count += 1

                # buyer - name
                partner_vals.update({'name': row[cell_count].decode('utf-8')})
                cell_count += 1

                # buyer - phone - number
                partner_vals.update({'phone': row[cell_count].decode('utf-8')})
                cell_count += 1

                # sku
                barcode = unicode(row[cell_count])
                # product = self.env['product.product'].search([('barcode', '=', unicode(row[cell_count]))])
                # order_line_vals.update({'product_id': product.id or False})
                cell_count += 1

                # name
                cell_count += 1

                # quantity - purchased
                order_line_vals.update({'product_uom_qty': unicode(row[cell_count])})
                cell_count += 1

                # currency
                currency = self.env['res.currency'].search([('name', '=', row[cell_count].decode('utf-8'))])
                order_line_vals.update({'currency_id': currency.id or False})
                cell_count += 1

                # price
                order_line_vals.update({'price_unit': unicode(row[cell_count])})
                cell_count += 1

                # item - tax
                cell_count += 1

                # shipping - price
                cell_count += 1


                # shipping - tax
                cell_count += 1

                # ship - service - level
                cell_count += 1

                # recipient - name
                shipping_vals.update({'name': row[cell_count].decode('utf-8')})
                cell_count += 1

                # ship - address - 1
                shipping_vals.update({'street': row[cell_count].decode('utf-8')})
                cell_count += 1

                # ship - address - 2
                shipping_vals.update({'street2': row[cell_count].decode('utf-8')})
                cell_count += 1

                # ship - address - 3
                #cell_count += 1

                # ship - city
                shipping_vals.update({'city': row[cell_count].decode('utf-8')})
                cell_count += 1

                # ship - state
                cell_count += 1

                # ship - postal - code
                shipping_vals.update({'zip': unicode(row[cell_count])})
                cell_count += 1

                # ship - country
                country = self.env['res.country'].search([('code', '=', unicode(row[cell_count]))])
                shipping_vals.update({'country_id': country.id or False})
                cell_count += 1

                # ship - phone - number
                shipping_vals.update({'phone': unicode(row[cell_count].decode('utf-8'))})
                cell_count += 1

                # item - promotion - discount
                cell_count += 1

                # item - promotion - id
                cell_count += 1

                # ship - promotion - discount
                cell_count += 1

                # ship - promotion - id
                cell_count += 1

                # delivery - start - date
                cell_count += 1

                # delivery - end - date
                cell_count += 1

                # delivery - time - zone
                cell_count += 1

                # delivery - Instructions
                cell_count += 1

                # sales - channel
                cell_count += 1

                # order - channel
                cell_count += 1

                # order - channel - instance
                cell_count += 1

                # external - order - id
                cell_count += 1

                # billing - name
                billing_vals.update({'name': row[cell_count].decode('utf-8')})
                cell_count += 1

                # billing - address - 1
                billing_vals.update({'street': row[cell_count].decode('utf-8')})
                cell_count += 1

                # billing - address - 2
                billing_vals.update({'street2': row[cell_count].decode('utf-8')})
                cell_count += 1

                # billing - address - 3
                billing_vals.update({'street2': billing_vals['street2'] + ' ' + row[cell_count].decode('utf-8')})
                cell_count += 1

                # billing - city
                billing_vals.update({'street2': row[cell_count].decode('utf-8')})
                cell_count += 1

                # billing - state
                cell_count += 1

                # billing - postal - code
                billing_vals.update({'zip': unicode(row[cell_count])})
                cell_count += 1

                # billing - country
                country = self.env['res.country'].search([('code', '=', unicode(row[cell_count]))])
                billing_vals.update({'country_id': country.id or False})
                cell_count += 1

                # billing - phone - number
                billing_vals.update({'phone': unicode(row[cell_count])})
                cell_count += 1

                # shipping - method
                cell_count += 1

                # promo - code
                cell_count += 1

                # color_text
                cell_count += 1

                # description
                order_line_vals.update({'name': row[cell_count].decode('utf-8')})
                cell_count += 1

                # info_circuit_ankle
                cell_count += 1

                # info_color
                cell_count += 1

                # info_decorations
                cell_count += 1

                # info_description
                cell_count += 1

                # info_hell_height
                cell_count += 1

                # info_inside
                cell_count += 1

                # info_insole
                cell_count += 1

                # info_material_hell
                cell_count += 1

                # info_platform_height
                cell_count += 1

                # info_sole
                cell_count += 1

                # info_sole_thickness
                cell_count += 1

                # info_total_height
                cell_count += 1

                # info_upper
                cell_count += 1

                # info_upper_circuit
                cell_count += 1

                # info_weight
                cell_count += 1

                # info_width
                cell_count += 1

                # meta_title
                cell_count += 1

                # product_sheep_catalog
                cell_count += 1

                # product_sheep_product
                cell_count += 1

                # product_wool_catalog
                cell_count += 1

                # product_wool_product
                cell_count += 1

                # short_description
                cell_count += 1

                # special_price
                cell_count += 1

                # url_key
                cell_count += 1

                # url_path
                cell_count += 1

                # _links_related_sku
                cell_count += 1

                # Partner
                partner = self.env['res.partner'].search([
                    ('name', '=', partner_vals['name']),
                    ('parent_id', '=', False)])
                if len(partner) == 0:
                    partner = self.env['res.partner'].create(partner_vals)
                else:
                    partner.write(partner_vals)
                order_vals.update({'partner_id': partner.id or False})

                # Shipping
                ship_partner = self.env['res.partner'].search([
                    ('name', '=', shipping_vals['name']),
                    ('type', '=', 'delivery')])
                shipping_vals.update({
                    'parent_id': partner.id
                })
                if len(ship_partner) == 0:
                    ship_partner = self.env['res.partner'].create(shipping_vals)
                else:
                    ship_partner.write(shipping_vals)

                # Billing
                bill_partner = self.env['res.partner'].search([
                    ('name', '=', billing_vals['name']),
                    ('type', '=', 'delivery')])
                billing_vals.update({
                    'parent_id': partner.id
                })
                if len(bill_partner) == 0:
                    bill_partner = self.env['res.partner'].create(billing_vals)
                else:
                    bill_partner.write(billing_vals)




                if len(product) == 1:
                    order = self.env['sale.order'].search([('name', '=', order_vals['name'])])
                    order_vals.update({'state': 'done'})
                    if len(order) == 0:
                        order = self.env['sale.order'].create(order_vals)
                    else:
                        order.write(order_vals)


                    order_line_vals.update({'product_uom': 1, 'order_id': order.id})
                    order_line = self.env['sale.order.line'].search([
                        ('order_id', '=', order.id),('product_id', '=', product.id)])
                    if len(order_line) == 0:
                        order_line = self.env['sale.order.line'].create(order_line_vals)
                    else:
                        order_line.write(order_line_vals)
                else:
                    print str(cont) + 'Magento_id ' + magento


            cont += 1

        os.remove('/tmp/shoe_embassy_online_orders.csv')
        return True


