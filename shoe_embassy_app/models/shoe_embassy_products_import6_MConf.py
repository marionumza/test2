# -*- coding: utf-8 -*-


from odoo import models, fields, osv, api
from odoo.exceptions import ValidationError
from datetime import datetime, date
import os
import unicodedata
import requests
from odoo.tools.translate import _
import csv
import base64

class ShoeEmbassyProductImport6MConf(models.Model):
    _name = "shoe.embassy.product.import6.mconf"

    name = fields.Char('Note')
    file = fields.Binary('Import Magento Conf File')
    filename = fields.Char()

    def shoe_embassy_product_import_from_csv(self):

        fo = open('/tmp/shoe_embassy_product6.csv', 'wb+')
        fo.write(base64.b64decode(self.file))
        fo.close()
        f = open('/tmp/shoe_embassy_product6.csv')
        csv_f = csv.reader(f)
        cont = 1
        headers = []
        error_count = 0
        for row in csv_f:
            print cont
            if cont == 1:
                cont += 1
                for r in row:
                    headers.append(r)
                continue
            else:
                cell_count = 0
                product_vals = {}

                #item-id
                product = self.env['product.template'].search([('conf_id', '=', unicode(row[cell_count]))])
                cell_count += 1

                # sku
                product_vals.update({'conf_sku': unicode(row[cell_count])})
                cell_count += 1

                # color_text
                cell_count += 1

                # description
                value = unicodedata.normalize('NFKD', row[cell_count].decode('utf-8')).encode('ascii', 'ignore')
                product_vals.update({'description': value})
                cell_count += 1

                # product_image
                r = requests.get(unicode(row[cell_count]))
                image_base64 = base64.encodestring(r.content)
                product_vals.update({'image': image_base64})
                cell_count += 1

                # info_circuit_ankle
                product_vals.update({'info_circuit_ankle': row[cell_count].decode('utf-8')})
                cell_count += 1

                # info_color
                info_colour = self.env['shoe.embassy.info_colour'].search([('name', '=', row[cell_count].decode('utf-8'))])
                if row[cell_count] and not info_colour.id:
                    info_colour = self.env['shoe.embassy.info_colour'].create({'name': row[cell_count].decode('utf-8')})
                product_vals.update({'info_colour_id': info_colour.id or False})
                cell_count += 1

                # info_decorations
                info_decoration = self.env['shoe.embassy.info_decoration'].search([('name', '=', row[cell_count].decode('utf-8'))])
                if row[cell_count] and not info_decoration.id:
                    info_decoration = self.env['shoe.embassy.info_decoration'].create({'name': row[cell_count].decode('utf-8')})
                product_vals.update({'info_decoration_id': info_decoration.id or False})
                cell_count += 1

                # info_description
                info_description = self.env['shoe.embassy.info_description'].search([('name', '=', row[cell_count].decode('utf-8'))])
                if row[cell_count] and not info_description.id:
                    info_description = self.env['shoe.embassy.info_description'].create({'name': row[cell_count].decode('utf-8')})
                product_vals.update({'info_description_id': info_description.id or False})
                cell_count += 1

                # info_hell_height
                info_heel_height = self.env['shoe.embassy.info_heel_height'].search([('name', '=', row[cell_count].decode('utf-8'))])
                if row[cell_count] and not info_heel_height.id:
                    info_heel_height = self.env['shoe.embassy.info_heel_height'].create({'name': row[cell_count].decode('utf-8')})
                product_vals.update({'info_heel_height_id': info_heel_height.id or False})
                cell_count += 1

                # info_inside
                info_inside = self.env['shoe.embassy.info_inside'].search([('name', '=', row[cell_count].decode('utf-8'))])
                if row[cell_count] and not info_inside.id:
                    info_inside = self.env['shoe.embassy.info_inside'].create({'name': row[cell_count].decode('utf-8')})
                product_vals.update({'info_inside_id': info_inside.id or False})
                cell_count += 1

                # info_insole
                info_insole = self.env['shoe.embassy.info_insole'].search([('name', '=', row[cell_count].decode('utf-8'))])
                if row[cell_count] and not info_insole.id:
                    info_insole = self.env['shoe.embassy.info_insole'].create({'name': row[cell_count].decode('utf-8')})
                product_vals.update({'info_insole_id': info_insole.id or False})
                cell_count += 1

                # info_material_heel
                material_heel = self.env['shoe.embassy.info.material_heel'].search([('name', '=', row[cell_count].decode('utf-8'))])
                if row[cell_count] and not material_heel.id:
                    material_heel = self.env['shoe.embassy.info.material_heel'].create({'name': row[cell_count].decode('utf-8')})
                product_vals.update({'info_material_heel_id': material_heel.id or False})
                cell_count += 1

                # info_platform_height
                info_platform_height = self.env['shoe.embassy.info_platform_height'].search([('name', '=', row[cell_count].decode('utf-8'))])
                if row[cell_count] and not info_platform_height.id:
                    info_platform_height = self.env['shoe.embassy.info_platform_height'].create({'name': row[cell_count].decode('utf-8')})
                product_vals.update({'info_platform_height_id': info_platform_height.id or False})
                cell_count += 1

                # info_sole
                info_sole = self.env['shoe.embassy.info_sole'].search([('name', '=', row[cell_count].decode('utf-8'))])
                if row[cell_count] and not info_sole.id:
                    info_sole = self.env['shoe.embassy.info_sole'].create({'name': row[cell_count].decode('utf-8')})
                product_vals.update({'info_sole_id': info_sole.id or False})
                cell_count += 1

                # info_sole_thickness
                info_sole_thickness = self.env['shoe.embassy.info_sole_thickness'].search([('name', '=', row[cell_count].decode('utf-8'))])
                if row[cell_count] and not info_sole_thickness.id:
                    info_sole_thickness = self.env['shoe.embassy.info_sole_thickness'].create({'name': row[cell_count].decode('utf-8')})
                product_vals.update({'info_sole_thickness_id': info_sole_thickness.id or False})
                cell_count += 1

                # info_total_height
                info_total_height = self.env['shoe.embassy.info_total_height'].search([('name', '=', row[cell_count].decode('utf-8'))])
                if row[cell_count] and not info_total_height.id:
                    info_total_height = self.env['shoe.embassy.info_total_height'].create({'name': row[cell_count].decode('utf-8')})
                product_vals.update({'info_total_height_id': info_total_height.id or False})
                cell_count += 1

                # info_upper
                info_upper = self.env['shoe.embassy.info_upper'].search([('name', '=', row[cell_count].decode('utf-8'))])
                if row[cell_count] and not info_upper.id:
                    info_upper = self.env['shoe.embassy.info_upper'].create({'name': row[cell_count].decode('utf-8')})
                product_vals.update({'info_upper_id': info_upper.id or False})
                cell_count += 1

                # info_upper_circuit
                product_vals.update({'info_upper_circuit': row[cell_count].decode('utf-8')})
                cell_count += 1

                # info_weight
                info_weight = self.env['shoe.embassy.info_weight'].search([('name', '=', row[cell_count].decode('utf-8'))])
                if row[cell_count] and not info_weight.id:
                    info_weight = self.env['shoe.embassy.info_weight'].create({'name': row[cell_count].decode('utf-8')})
                product_vals.update({'info_weight_id': info_weight.id or False})
                cell_count += 1

                # info_width
                info_width = self.env['shoe.embassy.info_width'].search([('name', '=', row[cell_count].decode('utf-8'))])
                if row[cell_count] and not info_width.id:
                    info_width = self.env['shoe.embassy.info_width'].create({'name': row[cell_count].decode('utf-8')})
                product_vals.update({'info_width_id': info_width.id or False})
                cell_count += 1

                # meta_title
                cell_count += 1

                # name/brand
                brand = self.env['shoe.embassy.brand'].search([('name', '=', row[cell_count].decode('utf-8'))])
                if row[cell_count] and not brand.id:
                    brand = self.env['shoe.embassy.brand'].create({'name': row[cell_count].decode('utf-8')})
                product_vals.update({'brand_id': brand.id or False})
                cell_count += 1

                # price
                product_vals.update({'list_price': unicode(row[cell_count])})
                cell_count += 1

                # product_sheep_catalog
                if unicode(row[cell_count]) not in ['Yes', 'No', '']:
                    raise ValidationError(_('The value product_sheep_catalog must be Yes or No. Line ' + str(cont)))
                product_vals.update({'product_sheep_catalog': row[cell_count].decode('utf-8')})
                cell_count += 1

                # product_sheep_product
                # if unicode(row[cell_count]) not in ['Yes', 'No', '']:
                #     raise ValidationError(_('The value product_sheep_product must be Yes or No. Line ' + str(cont)))
                # product_vals.update({'product_sheep_product': unicode(row[cell_count])})
                cell_count += 1

                # product_wool_catalog
                if unicode(row[cell_count]) not in ['Yes', 'No', '']:
                    raise ValidationError(_('The value product_wool_catalog must be Yes or No. Line ' + str(cont)))
                product_vals.update({'product_wool_catalog': row[cell_count].decode('utf-8')})
                cell_count += 1

                # product_wool_product
                # if unicode(row[cell_count]) not in ['Yes', 'No', '']:
                #     raise ValidationError(_('The value product_wool_product must be Yes or No. Line ' + str(cont)))
                # product_vals.update({'product_wool_product': unicode(row[cell_count])})
                cell_count += 1

                # short_description
                # product_vals.update({'short_description': unicode(row[cell_count])})
                cell_count += 1

                # special_price *********discounted price**************
                # product_vals.update({'special_price': unicode(row[cell_count])})
                cell_count += 1

                # url_key
                # product_vals.update({'url_key': unicode(row[cell_count])})
                cell_count += 1

                # url_path
                # product_vals.update({'url_path': unicode(row[cell_count])})
                cell_count += 1

                # _links_related_sku
                # product_vals.update({'_links_related_sku': unicode(row[cell_count])})
                cell_count += 1

                product_vals.update({'website_published': True})
                if len(product) == 0:
                    print "New product found"
                else:
                    try:
                        product.write(product_vals)
                    except:
                        raise ValidationError(_('Error occurs on the line ' + str(cont)))

            cont += 1
            error_count = 0

        os.remove('/tmp/shoe_embassy_product6.csv')
        return True



