# -*- coding: utf-8 -*-

from odoo import models, fields, api


class SizeCorrelation(models.Model):
    _name = 'size.correlation'

    length = fields.Float(string="Foot Length")
    uk = fields.Char(string='UK')
    us = fields.Char(string='US')
    eu = fields.Char(string='EU')
    gender_id = fields.Many2one('gender', string='Type')

