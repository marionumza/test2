# -*- coding: utf-8 -*-


from odoo import models, fields, osv, api
from odoo.exceptions import ValidationError
from datetime import datetime, date
import os


from odoo.tools.translate import _
from odoo.osv import osv
import json
import ast

import csv
import base64

class Shoe_Embassy_Newsite_import(models.Model):
    _name = "shoe.embassy.newsite.import"

    name = fields.Char('Note')
    file = fields.Binary('Import Newsite File')
    filename = fields.Char()

    

    def shoe_embassy_newsite_import_from_csv(self):

        fo = open('/tmp/shoe_embassy_newsite.csv', 'wb+')
        fo.write(base64.b64decode(self.file))
        fo.close()
        f = open('/tmp/shoe_embassy_newsite.csv')
        csv_f = csv.reader(f)
        cont = 1
        headers = []
        for row in csv_f:
            print cont
            if cont == 1:
                cont += 1
                for r in row:
                    headers.append(r)
                continue
            else:
                cell_count = 0
                message = ''

                # id
                product = self.env['product.product'].search([('id', '=', row[cell_count])])
                cell_count += 1

                # name
                cell_count += 1

                # New Site
                if unicode(row[cell_count]) not in ['yes', 'no', 'Yes', 'No', '']:
                    raise ValueError(_('Error on line ' + str(cont) + '. Cell ' + str(cell_count) + ' ' +
                                            headers[cell_count] + ' must be yes/no.'))
                else:
                    product.write({'new_site': unicode(row[cell_count]).lower()})




            cont += 1

        os.remove('/tmp/shoe_embassy_newsite.csv')
        self.write({'name': message})
        return True


