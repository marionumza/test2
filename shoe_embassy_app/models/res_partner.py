# -*- coding: utf-8 -*-

from odoo import models, fields, api, _, tools
from odoo.exceptions import ValidationError
import threading
from odoo.modules import get_module_resource
import random

import logging
_logger = logging.getLogger(__name__)

import datetime

import time

class Partner(models.Model):
    _inherit = 'res.partner'

    def _achived_target(self):
        pos_order_ids = self.env['pos.order'].search([('contact_type_id','=',self.id)])
        self.achived_target = sum([pos_id.amount_total for pos_id in pos_order_ids])

    @api.one
    def _compute_daily_target(self):
        target_comm_obj = self.env['sp.target.commission']
        target_comm_config_obj = self.env['target.commission.config']
        working_data = self.env['sp.working.data']
        daily_target_obj = self.env['daily.target']
        daily_target = 0.00
        daily_commission = 0.00
        working_data_search = working_data.search([('sperson_id.id','=',self.id),
                                                   ('work_date_from','<=',time.strftime("%Y-%m-%d %H:%M:%S")),
                                                   ('work_date_to','>=',time.strftime("%Y-%m-%d %H:%M:%S"))
                                                   ])
        if working_data_search:
            target_comm_search = target_comm_config_obj.search([
                                            ('date_single','=',time.strftime("%Y-%m-%d"))
                                           ])
            custom_target_comm_search = []
            if target_comm_search:
                for target_comm in target_comm_search:
                    selected_sperson = [sperson.id for sperson in target_comm.sp_custom_target_commission_id.pos_config_id.sperson_ids]
                    if self.id in selected_sperson:
                        custom_target_comm_search = target_comm
            
            
            today_date = str(datetime.datetime.today().date())
            
#             daily_target_search = daily_target_obj.search([('daily_date','>=',today_date + ' 00:00:00'),
#                                                            ('daily_date','<=',today_date + ' 23:59:59'),
#                                                            ])
            
#             if daily_target_search:
#                 print '____daily_target_search_____',daily_target_search
#                 for daily_target in daily_target_search:
#                     selected_sperson = [sperson.id for sperson in daily_target_search.sp_custom_target_commission_id.pos_config_id.sperson_ids]
#                     if self.id in selected_sperson:
#                         daily_target = 
                        
            
            #First it will search in Custom Target and Commission based on Current Date
            if custom_target_comm_search:
                for custom_target_comm in custom_target_comm_search:
                    
                    #This logic will check for commission, if commission is not defined then it will take from default_commission from res_partner 
                    if custom_target_comm.custom_commission == 0:
                        custom_commission = self.default_commission
                    else:
                        custom_commission = custom_target_comm.custom_commission
                    
                    if custom_target_comm.commission == 0:
                        commission = self.default_commission
                    else:
                        commission = custom_target_comm.commission
                    
                    if custom_target_comm.custom_hourly_rate:
                        daily_target += custom_target_comm.custom_hourly_rate * working_data_search.working_hours
                        if custom_target_comm.custom_commission:
                            daily_commission = daily_target * (custom_commission / 100.0)
                        else:
                            daily_commission = daily_target * (commission / 100.0)
                    else:
                        daily_target += custom_target_comm.hourly_rate * working_data_search.working_hours
                        if custom_target_comm.custom_commission:
                            daily_commission = daily_target * (custom_commission / 100.0)
                        else:
                            daily_commission = daily_target * (commission / 100.0)
            
            #else it will search in Target and Commission based on Week Day
            else:
                week_day = datetime.date.today().strftime("%A")
                target_comm_search_for_day = target_comm_config_obj.search([
                                    ('day_name','=',week_day.lower())
                                   ])
                if target_comm_search_for_day:
                    for daily_comm in target_comm_search_for_day:
                        selected_sperson = [sperson.id for sperson in daily_comm.sp_target_commission_id.pos_config_id.sperson_ids]
                        if self.id in selected_sperson:
                            daily_comm_search = daily_comm
                if daily_comm_search:
                    daily_target = daily_comm_search.hourly_rate * working_data_search.working_hours
                    
                    #check if commission not defined then take from default_commission
                    if daily_comm_search.commission == 0:
                        commission = self.default_commission
                    else:
                        commission = daily_comm_search.commission
                    daily_commission = daily_target * (commission / 100.00)
        
        self.daily_target = daily_target
        self.daily_commission = daily_commission
    
    @api.one
    def _compute_daily_achieved_target(self):
        daily_achieved_target = 0.00
        from datetime import datetime
        paid_sales_orders = self.env['sale.order'].search([('invoice_status','=','invoiced'),
                                                          ('partner_id','=',self.id)])
        if paid_sales_orders:
            for paid_sale_order in paid_sales_orders:
                standard_order_date = datetime.strptime(paid_sale_order.confirmation_date, '%Y-%m-%d %H:%M:%S')
                if str(standard_order_date.date()) == time.strftime("%Y-%m-%d"):
                    daily_achieved_target = paid_sale_order.amount_total
        self.daily_achieved_target = daily_achieved_target
    
    @api.one
    def _compute_weekly_target(self):
        target_comm_obj = self.env['sp.target.commission']
        target_comm_config_obj = self.env['target.commission.config']
        working_data = self.env['sp.working.data']
        from datetime import datetime, timedelta
        weekly_target = 0.00
        weekly_commission = 0.00
        start_date = datetime.now().date()
         
        for single_date in (start_date + timedelta(n) for n in range(7)):
            working_data_search = working_data.search([('sperson_id.id','=',self.id),
                                                   ('work_date_from','<=',str(single_date) + ' 23:59:59'),
                                                   ('work_date_to','>=',str(single_date) + ' 00:00:00')
                                                   ])
            if working_data_search:
                target_comm_search = target_comm_config_obj.search([
                                                ('date_single','=',single_date)
                                               ])
                custom_target_comm_search = []
                if target_comm_search:
                    for target_comm in target_comm_search:
                        selected_sperson = [sperson.id for sperson in target_comm.sp_custom_target_commission_id.pos_config_id.sperson_ids]
                        if self.id in selected_sperson:
                            custom_target_comm_search = target_comm
                
                #First it will search in Custom Target and Commission based on Current Date
                if custom_target_comm_search:
                    for custom_target_comm in custom_target_comm_search:
                        
                        #This logic will check for commission, if commission is not defined then it will take from default_commission from res_partner 
                        if custom_target_comm.custom_commission == 0:
                            custom_commission = self.default_commission
                        else:
                            custom_commission = custom_target_comm.custom_commission
                        
                        if custom_target_comm.commission == 0:
                            commission = self.default_commission
                        else:
                            commission = custom_target_comm.commission
                        
                        if custom_target_comm.custom_hourly_rate:
                            current_day_target = custom_target_comm.custom_hourly_rate * working_data_search.working_hours
                            weekly_target += custom_target_comm.custom_hourly_rate * working_data_search.working_hours
                            if custom_target_comm.custom_commission:
                                weekly_commission += current_day_target * (custom_commission / 100.0)
                            else:
                                weekly_commission += current_day_target * (commission / 100.0)
                        else:
                            current_day_target = custom_target_comm.hourly_rate * working_data_search.working_hours
                            weekly_target += custom_target_comm.hourly_rate * working_data_search.working_hours
                            if custom_target_comm.custom_commission:
                                weekly_commission += current_day_target * (custom_commission / 100.0)
                            else:
                                weekly_commission += current_day_target * (commission / 100.0)
                
                #else it will search in Target and Commission based on Week Day
                else:
                    target_comm_search_for_day = target_comm_config_obj.search([
                                                    ('day_name','=',single_date.strftime("%A").lower())
                                                   ])
                    if target_comm_search_for_day:
                        for weekly_comm in target_comm_search_for_day:
                            selected_sperson = [sperson.id for sperson in weekly_comm.sp_target_commission_id.pos_config_id.sperson_ids]
                            if self.id in selected_sperson:
                                weekly_comm_search = weekly_comm
                                
                                #check if commission not defined then take from default_commission
                                if weekly_comm_search.commission == 0:
                                    commission = self.default_commission
                                else:
                                    commission = weekly_comm_search.commission
                                
                                weekly_target += weekly_comm_search.hourly_rate * working_data_search.working_hours
                                current_day_target = weekly_comm_search.hourly_rate * working_data_search.working_hours
                                weekly_commission += current_day_target * (commission / 100.0)
        
        self.weekly_target = weekly_target
        self.weekly_commission = weekly_commission
    
    @api.one
    def _compute_weekly_achieved_target(self):
        so_obj = self.env['sale.order']
        from datetime import datetime, timedelta
        weekly_achieved_target = 0.00
         
        start_date = datetime.now().date()
        from_date = start_date.strftime("%Y-%m-%d")
         
        end_date = start_date + timedelta(days=7)
        to_date = end_date.strftime("%Y-%m-%d")
         
        paid_sales_orders = so_obj.search([('invoice_status','=','invoiced'),
                                           ('partner_id','=',self.id)
                                           ])
        if paid_sales_orders:
            for paid_sale_order in paid_sales_orders:
                import datetime
                order_date = datetime.datetime.strptime(paid_sale_order.confirmation_date, '%Y-%m-%d %H:%M:%S').date()
                if str(order_date) >= from_date and str(order_date) <= to_date:
                    weekly_achieved_target += paid_sale_order.amount_total
        self.weekly_achieved_target = weekly_achieved_target
        
    @api.one
    def _compute_monthly_target(self):
        target_comm_obj = self.env['sp.target.commission']
        target_comm_config_obj = self.env['target.commission.config']
        working_data = self.env['sp.working.data']
        from datetime import datetime, timedelta
        monthly_target = 0.00
        monthly_commission = 0.00
        start_date = datetime.now().date()
         
        for single_date in (start_date + timedelta(n) for n in range(30)):
            working_data_search = working_data.search([('sperson_id.id','=',self.id),
                                                   ('work_date_from','<=',str(single_date) + ' 23:59:59'),
                                                   ('work_date_to','>=',str(single_date) + ' 00:00:00')
                                                   ])
            if working_data_search:
                
                target_comm_search = target_comm_config_obj.search([
                                                ('date_single','=',single_date)
                                               ])
                custom_target_comm_search = []
                if target_comm_search:
                    for target_comm in target_comm_search:
                        selected_sperson = [sperson.id for sperson in target_comm.sp_custom_target_commission_id.pos_config_id.sperson_ids]
                        if self.id in selected_sperson:
                            custom_target_comm_search = target_comm
                
                
                if custom_target_comm_search:
                    for custom_target_comm in custom_target_comm_search:
                        
                        #This logic will check for commission, if commission is not defined then it will take from default_commission from res_partner 
                        if custom_target_comm.custom_commission == 0:
                            custom_commission = self.default_commission
                        else:
                            custom_commission = custom_target_comm.custom_commission
                        
                        if custom_target_comm.commission == 0:
                            commission = self.default_commission
                        else:
                            commission = custom_target_comm.commission
                        
                        
                        if custom_target_comm.custom_hourly_rate:
                            current_day_target = custom_target_comm.custom_hourly_rate * working_data_search.working_hours
                            monthly_target += custom_target_comm.custom_hourly_rate * working_data_search.working_hours
                            if custom_target_comm.custom_commission:
                                monthly_commission += current_day_target * (custom_commission / 100.0)
                            else:
                                monthly_commission += current_day_target * (commission / 100.0)
                        else:
                            current_day_target = custom_target_comm.hourly_rate * working_data_search.working_hours
                            monthly_target += custom_target_comm.hourly_rate * working_data_search.working_hours
                            if custom_target_comm.custom_commission:
                                monthly_commission += current_day_target * (custom_commission / 100.0)
                            else:
                                monthly_commission += current_day_target * (commission / 100.0)
                else:
                    target_comm_search_for_day = target_comm_config_obj.search([
                                                    ('day_name','=',single_date.strftime("%A").lower())
                                                   ])
                    if target_comm_search_for_day:
                        for monthly_comm in target_comm_search_for_day:
                            selected_sperson = [sperson.id for sperson in monthly_comm.sp_target_commission_id.pos_config_id.sperson_ids]
                            if self.id in selected_sperson:
                                monthly_comm_search = monthly_comm
                                
                                #check if commission not defined then take from default_commission
                                if monthly_comm_search.commission == 0:
                                    commission = self.default_commission
                                else:
                                    commission = monthly_comm_search.commission
                                
                                monthly_target += monthly_comm_search.hourly_rate * working_data_search.working_hours
                                current_day_target = monthly_comm_search.hourly_rate * working_data_search.working_hours
                                monthly_commission += current_day_target * (commission / 100.0)
        
        self.monthly_target = monthly_target
        self.monthly_commission = monthly_commission

    @api.one
    def _compute_monthly_achieved_target(self):
        so_obj = self.env['sale.order']
        from datetime import datetime, timedelta
        monthly_achieved_target = 0.00
         
        start_date = datetime.now().date()
        from_date = start_date.strftime("%Y-%m-%d")
         
        end_date = start_date + timedelta(days=30)
        to_date = end_date.strftime("%Y-%m-%d")
         
        paid_sales_orders = so_obj.search([('invoice_status','=','invoiced'),
                                           ('partner_id','=',self.id)
                                           ])
        if paid_sales_orders:
            for paid_sale_order in paid_sales_orders:
                import datetime
                order_date = datetime.datetime.strptime(paid_sale_order.confirmation_date, '%Y-%m-%d %H:%M:%S').date()
                if str(order_date) >= from_date and str(order_date) <= to_date:
                    monthly_achieved_target += paid_sale_order.amount_total
        self.monthly_achieved_target = monthly_achieved_target

    @api.one
    def _compute_past_30_target(self):
        
        target_comm_obj = self.env['sp.target.commission']
        target_comm_config_obj = self.env['target.commission.config']
        working_data = self.env['sp.working.data']
        from datetime import datetime, timedelta
        past_30_target = 0.00
        past_30_commission = 0.00
        start_date = datetime.now().date()
         
        for single_date in (start_date - timedelta(n) for n in range(30)):
            working_data_search = working_data.search([('sperson_id.id','=',self.id),
                                                   ('work_date_from','<=',str(single_date) + ' 23:59:59'),
                                                   ('work_date_to','>=',str(single_date) + ' 00:00:00')
                                                   ])
            if working_data_search:
                
                
                target_comm_search = target_comm_config_obj.search([
                                                ('date_single','=',single_date)
                                               ])
                custom_target_comm_search = []
                if target_comm_search:
                    for target_comm in target_comm_search:
                        selected_sperson = [sperson.id for sperson in target_comm.sp_custom_target_commission_id.pos_config_id.sperson_ids]
                        if self.id in selected_sperson:
                            custom_target_comm_search = target_comm
                
                
#                 custom_target_comm_search = target_comm_config_obj.search([
#                                                 ('sp_custom_target_commission_id.sperson_id.id','=',self.id),
#                                                 ('date_single','=',single_date)
#                                                ])
                 
                if custom_target_comm_search:
                    for custom_target_comm in custom_target_comm_search:
                        
                        #This logic will check for commission, if commission is not defined then it will take from default_commission from res_partner 
                        if custom_target_comm.custom_commission == 0:
                            custom_commission = self.default_commission
                        else:
                            custom_commission = custom_target_comm.custom_commission
                        
                        if custom_target_comm.commission == 0:
                            commission = self.default_commission
                        else:
                            commission = custom_target_comm.commission
                            
                        if custom_target_comm.custom_hourly_rate:
                            current_day_target = custom_target_comm.custom_hourly_rate * working_data_search.working_hours
                            past_30_target += custom_target_comm.custom_hourly_rate * working_data_search.working_hours
                            if custom_target_comm.custom_commission:
                                past_30_commission += current_day_target * (custom_commission / 100.0)
                            else:
                                past_30_commission += current_day_target * (commission / 100.0)
                        else:
                            current_day_target = custom_target_comm.hourly_rate * working_data_search.working_hours
                            past_30_target += custom_target_comm.hourly_rate * working_data_search.working_hours
                            if custom_target_comm.custom_commission:
                                past_30_commission += current_day_target * (custom_commission / 100.0)
                            else:
                                past_30_commission += current_day_target * (commission / 100.0)
                else:
                    target_comm_search_for_day = target_comm_config_obj.search([
                                                    ('day_name','=',single_date.strftime("%A").lower())
                                                   ])
                    if target_comm_search_for_day:
                        for past_30_comm in target_comm_search_for_day:
                            selected_sperson = [sperson.id for sperson in past_30_comm.sp_target_commission_id.pos_config_id.sperson_ids]
                            if self.id in selected_sperson:
                                past_30_comm_search = past_30_comm
                                
                                #check if commission not defined then take from default_commission
                                if past_30_comm_search.commission == 0:
                                    commission = self.default_commission
                                else:
                                    commission = past_30_comm_search.commission
                                
                                past_30_target += past_30_comm_search.hourly_rate * working_data_search.working_hours
                                current_day_target = past_30_comm_search.hourly_rate * working_data_search.working_hours
                                past_30_commission += current_day_target * (commission / 100.0)
        
        self.past_30_target = past_30_target
        self.past_30_commission = past_30_commission

    @api.one
    def _compute_past_30_achieved_target(self):
        so_obj = self.env['sale.order']
        from datetime import datetime, timedelta
        past_30_achieved_target = 0.00
         
        start_date = datetime.now().date()
        from_date = start_date.strftime("%Y-%m-%d")
         
        end_date = start_date - timedelta(days=30)
        to_date = end_date.strftime("%Y-%m-%d")
         
        paid_sales_orders = so_obj.search([('invoice_status','=','invoiced'),
                                           ('partner_id','=',self.id)
                                           ])
        if paid_sales_orders:
            for paid_sale_order in paid_sales_orders:
                import datetime
                order_date = datetime.datetime.strptime(paid_sale_order.confirmation_date, '%Y-%m-%d %H:%M:%S').date()
                if str(order_date) <= from_date and str(order_date) >= to_date:
                    past_30_achieved_target += paid_sale_order.amount_total
        self.past_30_achieved_target = past_30_achieved_target
    
    @api.one
    def _compute_yearly_target(self):
        
        target_comm_obj = self.env['sp.target.commission']
        target_comm_config_obj = self.env['target.commission.config']
        working_data = self.env['sp.working.data']
        from datetime import datetime, timedelta
        yearly_target = 0.00
        yearly_commission = 0.00
        start_date = datetime.now().date()
         
        for single_date in (start_date + timedelta(n) for n in range(365)):
            working_data_search = working_data.search([('sperson_id.id','=',self.id),
                                                   ('work_date_from','<=',str(single_date) + ' 23:59:59'),
                                                   ('work_date_to','>=',str(single_date) + ' 00:00:00')
                                                   ])
            if working_data_search:
                
                target_comm_search = target_comm_config_obj.search([
                                                ('date_single','=',single_date)
                                               ])
                custom_target_comm_search = []
                if target_comm_search:
                    for target_comm in target_comm_search:
                        selected_sperson = [sperson.id for sperson in target_comm.sp_custom_target_commission_id.pos_config_id.sperson_ids]
                        if self.id in selected_sperson:
                            custom_target_comm_search = target_comm
                
                if custom_target_comm_search:
                    for custom_target_comm in custom_target_comm_search:
                        
                        #This logic will check for commission, if commission is not defined then it will take from default_commission from res_partner 
                        if custom_target_comm.custom_commission == 0:
                            custom_commission = self.default_commission
                        else:
                            custom_commission = custom_target_comm.custom_commission
                        
                        if custom_target_comm.commission == 0:
                            commission = self.default_commission
                        else:
                            commission = custom_target_comm.commission
                        
                        if custom_target_comm.custom_hourly_rate:
                            current_day_target = custom_target_comm.custom_hourly_rate * working_data_search.working_hours
                            yearly_target += custom_target_comm.custom_hourly_rate * working_data_search.working_hours
                            if custom_target_comm.custom_commission:
                                yearly_commission += current_day_target * (custom_commission / 100.0)
                            else:
                                yearly_commission += current_day_target * (commission / 100.0)
                        else:
                            current_day_target = custom_target_comm.hourly_rate * working_data_search.working_hours
                            yearly_target += custom_target_comm.hourly_rate * working_data_search.working_hours
                            if custom_target_comm.custom_commission:
                                yearly_commission += current_day_target * (custom_commission / 100.0)
                            else:
                                yearly_commission += current_day_target * (commission / 100.0)
                else:
                    target_comm_search_for_day = target_comm_config_obj.search([
                                                    ('day_name','=',single_date.strftime("%A").lower())
                                                   ])
                    if target_comm_search_for_day:
                        for yearly_comm in target_comm_search_for_day:
                            selected_sperson = [sperson.id for sperson in yearly_comm.sp_target_commission_id.pos_config_id.sperson_ids]
                            if self.id in selected_sperson:
                                yearly_comm_search = yearly_comm
                                
                                #check if commission not defined then take from default_commission
                                if yearly_comm_search.commission == 0:
                                    commission = self.default_commission
                                else:
                                    commission = yearly_comm_search.commission
                                
                                yearly_target += yearly_comm_search.hourly_rate * working_data_search.working_hours
                                current_day_target = yearly_comm_search.hourly_rate * working_data_search.working_hours
                                yearly_commission += current_day_target * (commission / 100.0)
                    
        self.yearly_target = yearly_target
        self.yearly_commission = yearly_commission

    @api.one
    def _compute_yearly_achieved_target(self):
        so_obj = self.env['sale.order']
        from datetime import datetime, timedelta
        yearly_achieved_target = 0.00
         
        start_date = datetime.now().date()
        from_date = start_date.strftime("%Y-%m-%d")
         
        end_date = start_date + timedelta(days=365)
        to_date = end_date.strftime("%Y-%m-%d")
         
        paid_sales_orders = so_obj.search([('invoice_status','=','invoiced'),
                                           ('partner_id','=',self.id)
                                           ])
        if paid_sales_orders:
            for paid_sale_order in paid_sales_orders:
                import datetime
                order_date = datetime.datetime.strptime(paid_sale_order.confirmation_date, '%Y-%m-%d %H:%M:%S').date()
                if str(order_date) >= from_date and str(order_date) <= to_date:
                    yearly_achieved_target += paid_sale_order.amount_total
        self.yearly_achieved_target = yearly_achieved_target
    
    contact_type_id = fields.Many2one('shoe.embassy.contact_type', string='Contact Type')
    
    daily_target = fields.Float('Daily Sale Target', compute='_compute_daily_target')
    daily_commission = fields.Float('Daily Commission', compute='_compute_daily_target')
    daily_achieved_target = fields.Float('Daily Achieved Target', compute='_compute_daily_achieved_target')
    
    weekly_target = fields.Float('Weekly Sale Target', compute='_compute_weekly_target')
    weekly_commission = fields.Float('Weekly Commission Target', compute='_compute_weekly_target')
    weekly_achieved_target = fields.Float('Weekly Achieved Target', compute='_compute_weekly_achieved_target')
    
    monthly_target = fields.Float('Monthly Sale Target', compute='_compute_monthly_target')
    monthly_commission = fields.Float('Monthly Commission', compute='_compute_monthly_target')
    monthly_achieved_target = fields.Float('Monthly Achieved Target', compute='_compute_monthly_achieved_target')

    past_30_target = fields.Float('Past 30 Days Target', compute='_compute_past_30_target')
    past_30_commission = fields.Float('Past 30 Days Commission', compute='_compute_past_30_target')
    past_30_achieved_target = fields.Float('Past 30 Achieved Target', compute='_compute_past_30_achieved_target')
    
    yearly_target = fields.Float('Yearly Sale Target', compute='_compute_yearly_target')
    yearly_commission = fields.Float('Yearly Commission', compute='_compute_yearly_target')
    yearly_achieved_target = fields.Float('Yearly Achieved Target', compute='_compute_yearly_achieved_target')
    
    default_commission = fields.Float('Default Commission(%)')

    def users_from_clients(self, arg):
        partners = self.search(arg)
        cont = 0
        for partner in partners:
            cont += 1
            if partner.email:
                user = self.env['res.users'].search([('login', '=', partner.email)])
                if not user:
                    values = {
                        'login': partner.email,
                        'email': partner.email,
                        'partner_id': partner.id,
                        'groups_id': [(6, 0, [self.env['ir.model.data'].xmlid_to_res_id('base.group_portal') or False])],
                    }
                    self.env['res.users'].sudo().with_context(no_reset_password=True).create(values)
                    _logger.log(logging.INFO, 'User from Customer(%s) %s from %s CREATED', partner.email, cont, len(partners))
                else:
                    _logger.log(logging.INFO, 'User from Customer(%s) %s from %s EXIST', partner.email, cont, len(partners))
            else:
                _logger.log(logging.INFO, 'User from Customer ID(%s) %s from %s NOT CREATED', partner.id, cont, len(partners))
        return True
