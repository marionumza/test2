# -*- coding: utf-8 -*-


from odoo import models, fields, osv, api
from odoo.exceptions import ValidationError
from datetime import datetime, date
import os

from odoo.tools.translate import _
from odoo.osv import osv
import json
import ast

import csv
import base64

class ShoeEmbassyProductImport2Sort(models.Model):
    _name = "shoe.embassy.product.import2.sort"

    name = fields.Char('Note')
    file = fields.Binary('Import Sort File')
    filename = fields.Char()

    def shoe_embassy_product_import_from_csv(self):

        fo = open('/tmp/shoe_embassy_product2.csv', 'wb+')
        fo.write(base64.b64decode(self.file))
        fo.close()
        f = open('/tmp/shoe_embassy_product2.csv')
        csv_f = csv.reader(f)
        cont = 1
        headers = []
        error_count = 0
        for row in csv_f:
            print cont
            if cont == 1:
                cont += 1
                for r in row:
                    headers.append(r)
                continue
            else:
                cell_count = 0
                product_vals = {}
                variant_lining_vals = {}
                variant_lining1_vals = {}
                variant_lining2_vals = {}
                variant_lining3_vals = {}
                product_product_vals = {}

                # id
                cell_count += 1

                # name
                # product_vals.update({'name': row[cell_count].decode('utf-8')})
                # product_product_vals.update({'name': row[cell_count].decode('utf-8')})
                # product_vals.update({'default_code': unicode(row[cell_count])})
                cell_count += 1

                # barcode
                product_product_vals.update({'barcode': unicode(row[cell_count])})
                cell_count += 1

                # Problem
                cell_count += 1

                # Lifercycle
                life_cycle = self.env['shoe.embassy.life_cycle'].search([('name', '=', row[cell_count])])
                if row[cell_count] and not life_cycle.id:
                    life_cycle = self.env['shoe.embassy.life_cycle'].create({'name': row[cell_count].decode('utf-8')})
                product_vals.update({'life_cycle_id': life_cycle.id or False})
                cell_count += 1

                # Lining
                lining_list = []
                lining = self.env['shoe.embassy.lining'].search([('name', '=', row[cell_count])])
                if row[cell_count] and not lining.id:
                    lining = self.env['shoe.embassy.lining'].create({'name': row[cell_count].decode('utf-8')})
                if len(lining) > 0:
                    lining_list.append(lining.id)
                    variant_lining_vals.update({'attribute_name': 'Lining'})
                    variant_lining_vals.update({'value': unicode(row[cell_count])})
                cell_count += 1

                # lining 1
                lining1 = self.env['shoe.embassy.lining'].search([('name', '=', row[cell_count])])
                if row[cell_count] and not lining1.id:
                    lining1 = self.env['shoe.embassy.lining'].create({'name': row[cell_count].decode('utf-8')})
                if len(lining1) > 0:
                    lining_list.append(lining1.id)
                    variant_lining1_vals.update({'attribute_name': 'Lining'})
                    variant_lining1_vals.update({'value': unicode(row[cell_count])})
                cell_count += 1

                # Lining 2
                lining2 = self.env['shoe.embassy.lining'].search([('name', '=', row[cell_count])])
                if row[cell_count] and not lining2.id:
                    lining2 = self.env['shoe.embassy.lining'].create({'name': row[cell_count].decode('utf-8')})
                if len(lining2) > 0:
                    lining_list.append(lining2.id)
                    variant_lining2_vals.update({'attribute_name': 'Lining'})
                    variant_lining2_vals.update({'value': unicode(row[cell_count])})
                cell_count += 1

                if len(lining_list) > 0:
                    product_vals.update({'lining_ids': [(4, lining_list)]})

                # Gender
                category = self.env['product.category'].search([('name', '=', row[cell_count].strip().capitalize())])
                if row[cell_count] and not category.id:
                    category = self.env['product.category'].create({'name': unicode(row[cell_count].strip().capitalize())})
                product_vals.update({'sub_categ_id': category.id or False})
                website_categ_list = []
                # Website categories
                public_parent_category = self.env['product.public.category'].search([('name', '=', row[cell_count].strip().capitalize())])
                if row[cell_count] and not public_parent_category.id:
                    # public_parent_category = self.env['product.public.category'].search(
                    #     [('name', '=', row[cell_count][:-1])])
                    public_parent_category = self.env['product.public.category'].create({'name': unicode(row[cell_count].strip().capitalize())})
                if len(public_parent_category) > 0:
                    website_categ_list.append(public_parent_category.id)

                # POS categories
                pos_parent_category = self.env['pos.category'].search(
                    [('name', '=', row[cell_count].strip().capitalize())])
                if row[cell_count] and not pos_parent_category.id:
                    pos_parent_category = self.env['pos.category'].create(
                        {'name': unicode(row[cell_count].strip().capitalize())})
                cell_count += 1

                # categ_id / id
                cell_count += 1

                # Season
                if unicode(row[cell_count]) not in ['Summer', 'Spr-Aut', 'Winter', 'Mix', '']:
                    raise ValidationError(_('Error on line ' + str(cont) + '. Cell ' + str(cell_count) + ' ' +
                                            headers[cell_count] + ' must be Summer/Spr-Aut/Winter/Mix.'))
                product_vals.update({'season': row[cell_count].decode('utf-8')})
                cell_count += 1

                # Sale Category
                management_category = self.env['shoe.embassy.management_category'].search(
                    [('name', '=', row[cell_count])])
                if row[cell_count] and not management_category.id:
                    management_category = self.env['shoe.embassy.management_category'].create(
                        {'name': row[cell_count].decode('utf-8')})
                product_vals.update({'management_categ_id': management_category.id or False})
                cell_count += 1

                # Wall
                management_subcategory = self.env['shoe.embassy.management_category'].search(
                    [('name', '=', row[cell_count])])
                if row[cell_count] and not management_subcategory.id:
                    management_subcategory = self.env['shoe.embassy.management_category'].create({
                        'name': row[cell_count].decode('utf-8'),
                        'parent_id': management_category.id or False})
                product_vals.update({'management_subcateg_id': management_subcategory.id or False})
                cell_count += 1

                # Website Categories 1 (N)
                # Website category
                website_categ_list1 = None
                website_categ = self.env['product.public.category'].search(
                    [('name', '=', row[cell_count].strip().capitalize()), ('parent_id', '=', public_parent_category.id)])
                if row[cell_count] and not website_categ.id:
                    website_categ = self.env['product.public.category'].create({'name': unicode(row[cell_count].strip().capitalize()), 'parent_id':public_parent_category.id})
                if len(website_categ) > 0:
                    website_categ_list1 = website_categ.id
                # Carousel category
                product_vals.update({'carousel_categ_id': website_categ.id or False})

                # Pos Category
                pos_categ = self.env['pos.category'].search(
                    [('name', '=', row[cell_count].strip().capitalize()), ('parent_id', '=', pos_parent_category.id)])
                if row[cell_count] and not pos_categ.id:
                    pos_categ = self.env['pos.category'].create({'name': unicode(row[cell_count].strip().capitalize()), 'parent_id':pos_parent_category.id})
                product_vals.update({'pos_categ_id': pos_categ.id or False})
                cell_count += 1


                # Website Categories 2 (O)
                website_categ1 = None
                if row[cell_count]:
                    if website_categ:
                        website_categ1 = self.env['product.public.category'].search(
                            [('name', '=', row[cell_count].strip().capitalize()), ('parent_id', '=', website_categ.id)])
                        if not website_categ1:
                            website_categ1 = self.env['product.public.category'].create({'name': unicode(row[cell_count].strip().capitalize()), 'parent_id':website_categ.id})

                    if not website_categ and not website_categ1 and public_parent_category:
                        website_categ1 = self.env['product.public.category'].search(
                            [('name', '=', row[cell_count].strip().capitalize()), ('parent_id', '=', public_parent_category.id)])
                        if not website_categ1:
                            website_categ1 = self.env['product.public.category'].create({
                                'name': unicode(row[cell_count].strip().capitalize()), 'parent_id':public_parent_category.id})
                    if not website_categ1:
                        website_categ1 = self.env['product.public.category'].create({'name': unicode(row[cell_count].strip().capitalize())})
                    if len(website_categ1) > 0:
                        website_categ_list1 = website_categ1.id

                # Pos Category1
                pos_categ1 = None
                if row[cell_count]:
                    if pos_categ:
                        pos_categ1 = self.env['pos.category'].search(
                            [('name', '=', row[cell_count].strip().capitalize()), ('parent_id', '=', pos_categ.id)])
                        if not pos_categ1:
                            pos_categ1 = self.env['pos.category'].create({
                                'name': unicode(row[cell_count].strip().capitalize()), 'parent_id':pos_categ.id})
                    else:
                        pos_categ1 = self.env['pos.category'].search(
                            [('name', '=', row[cell_count].strip().capitalize()),
                             ('parent_id', '=', pos_parent_category.id)])
                        if not pos_categ1:
                            pos_categ1 = self.env['pos.category'].create({
                                'name': unicode(row[cell_count].strip().capitalize()),
                                'parent_id': pos_parent_category.id})

                    product_vals.update({'pos_categ_id': pos_categ1.id or False})
                cell_count += 1

                # Website Categories 1
                website_categ_list2 = None
                website_categ2 = self.env['product.public.category'].search(
                    [('name', '=', row[cell_count].strip().capitalize()), ('parent_id', '=', public_parent_category.id)])
                if row[cell_count] and not website_categ2.id:
                    website_categ2 = self.env['product.public.category'].create({'name': unicode(row[cell_count].strip().capitalize()), 'parent_id':public_parent_category.id})
                if len(website_categ2) > 0:
                    website_categ_list2 = website_categ2.id
                cell_count += 1

                # Website Categories 2
                website_categ3 = None
                if row[cell_count]:
                    if website_categ2:
                        website_categ3 = self.env['product.public.category'].search(
                            [('name', '=', row[cell_count].strip().capitalize()), ('parent_id', '=', website_categ2.id)])
                        if not website_categ3:
                            website_categ3 = self.env['product.public.category'].create({'name': unicode(row[cell_count].strip().capitalize()), 'parent_id':website_categ2.id})

                    if not website_categ2 and not website_categ3 and public_parent_category:
                        website_categ3 = self.env['product.public.category'].search(
                            [('name', '=', row[cell_count].strip().capitalize()), ('parent_id', '=', public_parent_category.id)])
                        if not website_categ3:
                            website_categ3 = self.env['product.public.category'].create({'name': unicode(row[cell_count].strip().capitalize()), 'parent_id':public_parent_category.id})
                    if not website_categ3:
                        website_categ3 = self.env['product.public.category'].create({'name': unicode(row[cell_count].strip().capitalize())})
                    if len(website_categ3) > 0:
                        website_categ_list2 = website_categ3.id
                cell_count += 1

                website_categ_list = []
                if website_categ_list1:
                    website_categ_list.append(website_categ_list1)
                if website_categ_list2:
                    website_categ_list.append(website_categ_list2)
                if not website_categ_list1 and not website_categ_list2 and website_categ_list:
                    website_categ_list.append(website_categ_list)


                if len(website_categ_list) > 0:
                    product_vals.update({'public_categ_ids': [(4, website_categ_list)]})

                # Performance
                performance = self.env['shoe.embassy.performance'].search([('name', '=', row[cell_count])])
                if row[cell_count] and not performance.id:
                    performance = self.env['shoe.embassy.performance'].create({'name': row[cell_count].decode('utf-8')})
                product_vals.update({'performance_id': performance.id or False})
                cell_count += 1

                # Special category
                special_categ = self.env['shoe.embassy.special_category'].search([('name', '=', row[cell_count])])
                if row[cell_count] and not special_categ.id:
                    special_categ = self.env['shoe.embassy.special_category'].create({'name': row[cell_count].decode('utf-8')})
                product_vals.update({'special_categ_id': special_categ.id or False})
                cell_count += 1

                # Outlet
                outlet = self.env['shoe.embassy.outlet'].search([('name', '=', row[cell_count])])
                if row[cell_count] and not outlet.id:
                    outlet = self.env['shoe.embassy.outlet'].create({'name': row[cell_count].decode('utf-8')})
                product_vals.update({'outlet_id': outlet.id or False})
                cell_count += 1

                # New Site
                if unicode(row[cell_count]) not in ['yes', 'no', 'Yes', 'No', '']:
                    raise ValidationError(_('Error on line ' + str(cont) + '. Cell ' + str(cell_count) + ' ' +
                                            headers[cell_count] + ' must be yes/no.'))
                product_vals.update({'new_site': unicode(row[cell_count]).lower()})
                cell_count += 1

                ######################################################################################################

                # PRODUCT update block
                # product = self.env['product.template'].search([('name', '=', product_vals['name'])])
                product_product = self.env['product.product'].search([('barcode', '=', product_product_vals['barcode'])])
                product = product_product.product_tmpl_id
                if len(product) == 0:
                    print "missing template for " + product_product_vals['barcode']
                    #product = self.env['product.template'].create(product_vals)
                else:
                    product.write(product_vals)

                # attribute block Lining
                attribute_lining = self.env['product.attribute'].search(
                    [('name', '=', 'Lining')])
                if len(attribute_lining) == 0:
                    attribute_lining = self.env['product.attribute'].create({'name': 'Lining'})

                    if 'value' in variant_lining_vals:
                        value_lining = self.env['product.attribute.value'].search([
                            ('name', '=', variant_lining_vals['value']),
                            ('attribute_id', '=', attribute_lining.id)])
                        if variant_lining_vals['value'] and len(value_lining) == 0:
                            value_lining = self.env['product.attribute.value'].create({
                                'name': variant_lining_vals['value'],
                                'attribute_id': attribute_lining.id})

                        if attribute_lining and value_lining:
                            line = self.env['product.attribute.line'].search([
                                ('attribute_id', '=', attribute_lining.id),
                                ('product_tmpl_id', '=', product.id)])
                            if len(line) == 0:
                                line = self.env['product.attribute.line'].create({
                                    'attribute_id': attribute_lining.id,
                                    'product_tmpl_id': product.id,
                                    'value_ids': [(4, value_lining.id)]})
                            else:
                                line.write({'value_ids': [(4, value_lining.id)]})
                            product.write({'attribute_line_ids': [(4, line.id)]})

                    if 'value' in variant_lining1_vals:
                        # lining1  ____________________________________________________________________________________
                        value_lining1 = self.env['product.attribute.value'].search([
                            ('name', '=', variant_lining1_vals['value']),
                            ('attribute_id', '=', attribute_lining.id)])
                        if variant_lining1_vals['value'] and len(value_lining1) == 0:
                            value_lining1 = self.env['product.attribute.value'].create({
                                'name': variant_lining1_vals['value'],
                                'attribute_id': attribute_lining.id})

                        if attribute_lining and value_lining1:
                            line = self.env['product.attribute.line'].search([
                                ('attribute_id', '=', attribute_lining.id),
                                ('product_tmpl_id', '=', product.id)])
                            if len(line) == 0:
                                line = self.env['product.attribute.line'].create({
                                    'attribute_id': attribute_lining.id,
                                    'product_tmpl_id': product.id,
                                    'value_ids': [(4, value_lining1.id)]})
                            else:
                                line.write({'value_ids': [(4, value_lining1.id)]})
                            product.write({'attribute_line_ids': [(4, line.id)]})

                    if 'value' in variant_lining2_vals:
                        # lining2  ________________________________________________________________________________
                        value_lining2 = self.env['product.attribute.value'].search([
                            ('name', '=', variant_lining2_vals['value']),
                            ('attribute_id', '=', attribute_lining.id)])
                        if variant_lining2_vals['value'] and len(value_lining2) == 0:
                            value_lining2 = self.env['product.attribute.value'].create({
                                'name': variant_lining2_vals['value'],
                                'attribute_id': attribute_lining.id})

                        if attribute_lining and value_lining2:
                            line = self.env['product.attribute.line'].search([
                                ('attribute_id', '=', attribute_lining.id),
                                ('product_tmpl_id', '=', product.id)])
                            if len(line) == 0:
                                line = self.env['product.attribute.line'].create({
                                    'attribute_id': attribute_lining.id,
                                    'product_tmpl_id': product.id,
                                    'value_ids': [(4, value_lining2.id)]})
                            else:
                                line.write({'value_ids': [(4, value_lining2.id)]})
                            product.write({'attribute_line_ids': [(4, line.id)]})

            cont += 1
            error_count = 0

        os.remove('/tmp/shoe_embassy_product2.csv')
        return True