# -*- coding: utf-8 -*-

from odoo import models, fields, tools, api


class ProductAttributevalueInherit(models.Model):

    _inherit = 'product.attribute.value'

    is_active = fields.Boolean('Active')
    is_used = fields.Boolean('Used')


class ProductAttributeInherit(models.Model):

    _inherit = 'product.attribute'

    is_active = fields.Boolean('Active')