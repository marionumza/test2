# -*- coding: utf-8 -*-

from odoo import api, fields, models


class ShoeEmbassyBrand(models.Model):
    _name = 'shoe.embassy.brand'
    _order = 'name'

    name = fields.Char(string='Brand')


class ProductImage(models.Model):
    _name = 'product.image'
    _description = 'Product Image'
    _order = 'type'
    
    name = fields.Char(string='Name')
    description = fields.Text(string='Description')
    image_alt = fields.Text(string='Image Label')
    image = fields.Binary(string='Image')
    is_active = fields.Boolean('Active', default=True)
#     image = fields.Binary(compute='_image_store_function_field',string='image',store=False)
    image_small = fields.Binary(string='Small Image')
    image_url = fields.Char(string='Image URL')
    type = fields.Selection( [('c','Category'),
                              ('p1','Side Front'),
                              ('p2','Side Left'),
                              ('p3','Top'),
                              ('p4','Bottom'),
                              ('p5','Side Back'),
                              ('p6','2 Shoes'),
                              ('p7','Product 7'),
                              ('p8','Product 8'),
                              ('p9','Product 9'),
                              ('p10','Product 10'),
                              ('p11','Product 11'),
                              ('p12','Product 12'),
                              ('p13','Product 13'),
                              ('p14','Product 14'),
                              ('p15','Product 15')
                              ],'Type')
    product_tmpl_id = fields.Many2one('product.template', 'Product',
                                      copy=False)
    product_variant_id = fields.Many2one('product.product', 'Product Variant',
                                         copy=False)


class ShoeEmbassyColour(models.Model):
    _name = 'shoe.embassy.colour'

    name = fields.Char(string='Colour')


class ShoeEmbassyColourCode(models.Model):
    _name = 'shoe.embassy.colour.code'

    colour_id = fields.Many2one('shoe.embassy.colour', string='Colour')
    name = fields.Char(string='Colour Code')


class ShoeEmbassyColourText(models.Model):
    _name = 'shoe.embassy.colour_text'

    name = fields.Char(string='Colour Text')


class ShoeEmbassyInfoColour(models.Model):
    _name = 'shoe.embassy.info_colour'

    name = fields.Char(string='Info Colour')


class ShoeEmbassyInfoDecoration(models.Model):
    _name = 'shoe.embassy.info_decoration'

    name = fields.Char(string='Info Decoration')


class ShoeEmbassyInfoDescription(models.Model):
    _name = 'shoe.embassy.info_description'

    name = fields.Char(string='Info Description')


class ShoeEmbassyInfoHeelHeight(models.Model):
    _name = 'shoe.embassy.info_heel_height'

    name = fields.Char(string='Info Heel Height')


class ShoeEmbassyInfoInside(models.Model):
    _name = 'shoe.embassy.info_inside'

    name = fields.Char(string='Info Inside')


class ShoeEmbassyInfoInsole(models.Model):
    _name = 'shoe.embassy.info_insole'

    name = fields.Char(string='Info Insole')


class ShoeEmbassyInfoMaterialHeel(models.Model):
    _name = 'shoe.embassy.info.material_heel'

    name = fields.Char(string='Info Material Heel')


class ShoeEmbassyInfoPlatformHeight(models.Model):
    _name = 'shoe.embassy.info_platform_height'

    name = fields.Char(string='Info Platform Height')


class ShoeEmbassyInfoSole(models.Model):
    _name = 'shoe.embassy.info_sole'

    name = fields.Char(string='Info Sole')


class ShoeEmbassyInfoSoleThickness(models.Model):
    _name = 'shoe.embassy.info_sole_thickness'

    name = fields.Char(string='Info Sole Thickness')


class ShoeEmbassyInfoTotalHeight(models.Model):
    _name = 'shoe.embassy.info_total_height'

    name = fields.Char(string='Info Total Height')


class ShoeEmbassyInfoUpper(models.Model):
    _name = 'shoe.embassy.info_upper'

    name = fields.Char(string='Info Upper')


class ShoeEmbassyInfoWeight(models.Model):
    _name = 'shoe.embassy.info_weight'

    name = fields.Char(string='Info Weight')


class ShoeEmbassyInfoWidth(models.Model):
    _name = 'shoe.embassy.info_width'

    name = fields.Char(string='Info Width')


class ShoeEmbassyShoeType(models.Model):
    _name = 'shoe.embassy.shoe_type'

    name = fields.Char(string='Shoe Type')

class  ProductTemplate(models.Model):
    
    _inherit = 'product.template'
    
    brand_id = fields.Many2one('shoe.embassy.brand', string='Brand Name')
    colour_id = fields.Many2one('shoe.embassy.colour', string='Colour')
    gender = fields.Selection([('Womens', 'Womens'), ('Mens', 'Mens')], string='Gender M/F')
    variant_bool = fields.Boolean('Variant Bool')
    images = fields.One2many('product.image', 'product_tmpl_id', 'Images')
    info_description_id = fields.Many2one('shoe.embassy.info_description', string='Info Description')
    colour_text_id = fields.Many2one('shoe.embassy.colour_text', string='Colour Text')
    info_colour_id = fields.Many2one('shoe.embassy.info_colour', string='Info colour')
    info_decoration_id = fields.Many2one('shoe.embassy.info_decoration', string='Info Decoration')

    info_heel_height_id = fields.Many2one('shoe.embassy.info_heel_height', string='Info Heel Height')
    info_inside_id = fields.Many2one('shoe.embassy.info_inside', string='Info Inside')
    info_insole_id = fields.Many2one('shoe.embassy.info_insole', string='Info Insole')
    info_material_heel_id = fields.Many2one('shoe.embassy.info.material_heel', string='Info Material Heel')
    info_platform_height_id = fields.Many2one('shoe.embassy.info_platform_height', string='Info Platform Height')
    info_sole_id = fields.Many2one('shoe.embassy.info_sole', string='Info Sole')
    info_sole_thickness_id = fields.Many2one('shoe.embassy.info_sole_thickness', string='Info Sole Thickness')
    info_total_height_id = fields.Many2one('shoe.embassy.info_total_height', string='Info Total Height')
    info_upper_id = fields.Many2one('shoe.embassy.info_upper', string='Info Upper')
    info_upper_circuit = fields.Char(string='Info Upper Circuit')
    info_width_id = fields.Many2one('shoe.embassy.info_width', string='Info Width')
    info_weight_id = fields.Many2one('shoe.embassy.info_weight', string='Info Weight')
    info_circuit_ankle = fields.Char(string='Info Height')
    product_sheep_catalog = fields.Selection([('Yes', 'Yes'), ('No', 'No')], string='Sheep Catalog')
    product_wool_catalog = fields.Selection([('Yes', 'Yes'), ('No', 'No')], string='Wool Catalog')
    shoe_type_id = fields.Many2one('shoe.embassy.shoe_type', string='Shoe Type')


    life_cycle_id = fields.Many2one('shoe.embassy.life_cycle', string='Life Cycle')